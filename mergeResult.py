#!/usr/bin/env python
# -*- coding:utf -*-
"""
#Created by Esteban.

Mon 01 Aug 2016 01:58:40 AM CEST

"""

import pandas as pd
import numpy as np

base = pd.read_csv("results_merge/heat_base.csv", index_col=2, usecols=[1,2,3])
s1 = pd.read_csv("results_merge/heat_s1.csv",     index_col=2, usecols=[1,2,3])
s2 = pd.read_csv("results_merge/heat_s2.csv",     index_col=2, usecols=[1,2,3])
s3 = pd.read_csv("results_merge/heat_s3.csv",     index_col=2, usecols=[1,2,3])

data = pd.merge(base, s1, how='inner', right_index=True, left_index=True, suffixes=('', '_s1'))
data = pd.merge(data, s2, how='inner', right_index=True, left_index=True, suffixes=('', '_s2'))
data = pd.merge(data, s3, how='inner', right_index=True, left_index=True, suffixes=('', '_s3'))

tpop = pd.read_csv("data/tpop.csv")
tpop.index = ["{:012.0f}".format(a) for a in tpop.area_id]

tindex = tpop.index.tolist()
tindex[1116] = '020000000000'
tindex[8480] = '110000000000'
tindex[2141] = '040110000000'
tindex[2142] = '040120000000'
tpop.index = tindex

bundesland = [a[0:2] for a in tpop.index.tolist()]
tpop.loc[:, "bundesland"] = bundesland

geodata = pd.read_csv("data/VG250_Gemeinden_new.csv")
geodata.index = ["{:012.0f}".format(a) for a in geodata.RS_ALT]

data2 = pd.merge(tpop.loc[:,["pop", "du", "building", "bundesland"]],
                 geodata.loc[:,["OBJECTID", "region"]],
                 right_index=True, left_index=True)
data2 = data2.set_index("OBJECTID")

data_all = pd.merge(data, data2, how='inner', right_index=True, left_index=True)

data_all.loc[:, 'CO2']     = data_all.loc[:, 'CO2'    ].div(data_all.loc[:, 'pop'])
data_all.loc[:, 'CO2_s1']  = data_all.loc[:, 'CO2_s1' ].div(data_all.loc[:, 'pop'])
data_all.loc[:, 'CO2_s2']  = data_all.loc[:, 'CO2_s2' ].div(data_all.loc[:, 'pop'])
data_all.loc[:, 'CO2_s3']  = data_all.loc[:, 'CO2_s3' ].div(data_all.loc[:, 'pop'])
data_all.loc[:, 'heat']    = data_all.loc[:, 'heat'   ].div(data_all.loc[:, 'pop'] * 1000)
data_all.loc[:, 'heat_s1'] = data_all.loc[:, 'heat_s1'].div(data_all.loc[:, 'pop'] * 1000)
data_all.loc[:, 'heat_s2'] = data_all.loc[:, 'heat_s2'].div(data_all.loc[:, 'pop'] * 1000)
data_all.loc[:, 'heat_s3'] = data_all.loc[:, 'heat_s3'].div(data_all.loc[:, 'pop'] * 1000)

data_all = data_all.loc[data_all.heat != 0, :]

data_all.to_csv("results_merge/all.csv")
