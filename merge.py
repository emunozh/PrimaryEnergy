#!/usr/bin/env python
# -*- coding:utf -*-
"""
#Created by Esteban.

Sat 30 Jul 2016 10:23:44 PM CEST

"""

import sys
from root.Climate.merge import mergeData

case = sys.argv[1]

mergeData(output="Year", res_file="heat_{}.csv".format(case),
          results_heat_path="results_heat_{}".format(case))
