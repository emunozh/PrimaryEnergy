#!/usr/bin/env python
# -*- coding:utf -*-
"""
#Created by Esteban.

Sat 30 Jul 2016 06:04:53 PM CEST

"""


from root.Climate.ResultsGeo import Map

UN = "MWh/cap*a"
UC = "heat"
F  = 1
IC = 0

def plotMap(s, uc, unit):
    M = Map(
        heatData="./results_merge/all.csv",
        index_col=IC, unit=unit, factor=F, useCol=uc,
            )
    M.makeYear(sufix=s, perArea=False)

def main():
    #scenarions = ["base"]
    units = ["MWh/cap*a"]*4
    units.extend(["tonnes/cap*a"]*4)
    scenarions = ["base", "s1", "s2", "s3"]
    UCS = ["heat", "heat_s1", "heat_s2", "heat_s2",
           "CO2", "CO2_s1", "CO2_s2", "CO2_s3"]
    for s, uc, unit in zip(scenarions*2, UCS, units*2):
        print(s, uc, unit)
        plotMap(s, uc, unit)
    #plotMap("base", "heat", "MWh/cap*a")
    # plotMap("./results_merge/heat_s1.csv", "")
    # plotMap("./results_merge/heat_s2.csv")
    # plotMap("./results_merge/heat_s3.csv")

if __name__ == "__main__":
    main()
