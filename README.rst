.. header::
    ###Title### - page ###Page###/###Total###

.. footer::
    Muñoz & Peters - Tue 12 Jul 2016

===============================================================================================================
Estimating Primary Energy Demand of Domestic Heat Consumption in Germany with Microdata (supplementary material)
===============================================================================================================

:Author: `M. Esteban Munoz H.`_
:Date: Tue 12 Jul 2016
:Prepared for: `Urban Transitions Global Summit 2016`__
:Ipython Notebook: notebook_

.. _`M. Esteban Munoz H.`: marcelo.hidalgo@hcu-hamburg.de
__ http://www.urbantransitionsconference.com/
.. _notebook: http://nbviewer.jupyter.org/urls/gitlab.com/emunozh/PrimaryEnergy/raw/master/log_reg_pe.ipynb


.. contents:: Table of Contents
   :depth: 4

.. topic:: Abstract

    Spatial Microsimulation methods are used to enable the statistical modeling
    at a micro level with a spatial reference.
    The core idea of spatial microsimulation models relies on the generation of
    a synthetic population representing the population of small geographical
    areas. We have expand this method in order to generate not only a synthetic
    population but a synthetic building stock. This addition to the method
    allows us to model urban environments with the explicit consideration of
    building stock dynamics.
    The consideration of building characteristics is important (among other
    applications) for the estimation of heat demand.

    In this paper we present a method for the integration of primary energy
    factors, needed for the estimation of primary energy used for domestic
    heating in Germany.
    This is an important factor to consider for the estimation of CO_2
    emissions and the analysis of policies aiming to reduce carbon emissions, A
    topic that has been a high priority for cities in Germany and it is
    expected to be intensify by the new Paris COP-21 agreements.

    Aim of our research is to develop policy assessment models able to capture
    both: (a) the physical properties of the building stock and (b) demographic
    characteristics of the residents. Both parameters are important for the
    development of tools and models aiming to asses policies targeting a reduction
    of CO_2 emissions of the build environment. An integration of these models with
    demographic information allows us to: (a) asses the impact of policies at a
    social level and (b) develop more complex models taking social aspects for the
    projection of retrofits investments and impact on heat consumption. The
    description of the physical characteristics of the building stock allow us to
    asses the introduction of new technologies and quantify the effect of a large
    scale retrofit agenda at a material level.

    In this paper we reweight the 2010 German microcensus to available aggregated
    (NUTS-2) small area statistics, from the 2011 German census. We compute the
    primary energy demand for each individual in the microcensus. We use a GREGWT
    implementation for the reweighting process which allows us to reweight the
    microcensus at three aggregation levels: (1) people/individuals, (2)
    households/dwelling units and (3) Buildings. For the computation of heat demand
    we use a quasi steady state implementation of the German DIN 18599.

    The validation of these simulation is a challenge. There are two methods
    describe in the literature for the validation of spatial microsimulation
    models: (1) a validation at a microlevel and (2) a validation at an aggregated
    level. In this case we don't have proper data at neither of these levels. Heat
    monitoring data at a low aggregation level for a representative sample (not
    extremely efficient pilot projects) is hard to get and available aggregated
    statistics of energy consumption do not distinguish between the domestic and
    tertiary sector.

    In this paper we want to present a different validation method. Since the
    introduction of the renewable energy law EEG in Germany every energy production
    facility has to be list by location and capacity. We analyse the relationship
    between estimated fuel type and capacity density of specific renewable sources
    in the same geographical area.

    Results in these paper are: (1) a model able to estimate the primary energy
    demand of heat supply for the residential sector at a microlevel, spatially
    referenced; (2) a model integrating demographic characteristics into the
    synthetic building stock, opening the doors for a wide range of analysis; and
    (3) a validation method for models estimating heat demand of geographical
    areas.

--------------------------------------------

Simulation Tools
================

1. Reweighting the microcensus https://github.com/emunozh/GREGWT
2. Estimating heat demand https://github.com/emunozh/heat

Meta-Data
================

Microsensus 2010 (Rewighted Microdata):
----------------------------------------------

.. _Tab.1:

.. table:: Table 1: Used simulation parameters within the model

   +-----+-------+-------------+-----------------------+-------------------+-------------+-----------+
   | reg | bench | Variable ID | German Name           | English           | Aggregation | Tab       |
   |     |       |             |                       | Translation       | Level       |           |
   +=====+=======+=============+=======================+===================+=============+===========+
   | X1  |       | **EF1**     | Bundesland            | Federal State     | Individual  | Tab.2_    |
   |     |       |             |                       | (NUTS-2)          |             |           |
   +-----+-------+-------------+-----------------------+-------------------+-------------+-----------+
   |     |       | **EF952**   |                       | Weight            | Individual  |           |
   +-----+-------+-------------+-----------------------+-------------------+-------------+-----------+
   |     | b.1   | **EF44**    |                       | Age               | Individual  |           |
   +-----+-------+-------------+-----------------------+-------------------+-------------+-----------+
   |     | b.2   | **EF49**    |                       | Marital Status    | Individual  |           |
   +-----+-------+-------------+-----------------------+-------------------+-------------+-----------+
   |     | b.3   | **EF46**    |                       | Sex               | Individual  |           |
   +-----+-------+-------------+-----------------------+-------------------+-------------+-----------+
   |     | b.4   | **EF20**    |                       | Household size    | Individual  |           |
   +-----+-------+-------------+-----------------------+-------------------+-------------+-----------+
   |     | b.5   | **EF368**   |                       | Citizenship       | Individual  |           |
   +-----+-------+-------------+-----------------------+-------------------+-------------+-----------+
   |     | b.6   | **EF492**   |                       | Floor area        | Household   |           |
   |     |       |             |                       |                   | (DU)        |           |
   +-----+-------+-------------+-----------------------+-------------------+-------------+-----------+
   |     |       | **EF131**   | Normale Arbeitszeit   | Working hours     |             | Tab.11_   |
   +-----+-------+-------------+-----------------------+-------------------+-------------+-----------+
   |     |       | **EF171**   | Heimarbeit            | Working at home   |             | Tab.12_   |
   +-----+-------+-------------+-----------------------+-------------------+-------------+-----------+
   |     |       | **EF491**   | Eigentum der          | Ownership of the  | Household   | Tab.10_   |
   |     |       |             | Mietwohnung           | renting dwelling  | (DU)        |           |
   +-----+-------+-------------+-----------------------+-------------------+-------------+-----------+
   |     | b.8   | **EF809**   | Typ der Lebensform    | Type of family    | Household   | Tab.9_    |
   |     |       |             |                       |                   | (DU)        |           |
   +-----+-------+-------------+-----------------------+-------------------+-------------+-----------+
   |     | b.9   | **EF635**   |                       | Number of         | Building    |           |
   |     |       |             |                       | Dwellings         |             |           |
   +-----+-------+-------------+-----------------------+-------------------+-------------+-----------+
   | X2  | b.10  | **EF494**   | Baujahr der Wohnung   | Construction Year | Building    | Tab.3_    |
   +-----+-------+-------------+-----------------------+-------------------+-------------+-----------+
   | X3  | b.11  | **EF496**   | Überw. Beheizung      | Heating typ       | Household   | Tab.4_    |
   |     |       |             | der Wohnung           | renting dwelling  | (DU)        |           |
   +-----+-------+-------------+-----------------------+-------------------+-------------+-----------+
   | X4  |       | **EF501**   | Warme Nebenkosten:    | Heat Expenditure  | Household   | Tab.5a_ & |
   |     |       |             | Höhe                  | renting dwelling  | (DU)        | Tab.5b_   |
   +-----+-------+-------------+-----------------------+-------------------+-------------+-----------+
   | X5  |       | **EF563**   | Gemeindegrößenklasse  | Small Area size   | Small Area  | Tab.6_    |
   +-----+-------+-------------+-----------------------+-------------------+-------------+-----------+
   | X6  |       | **EF570**   | Gebäudegrößenklasse   | Building size     | Building    | Tab.7_    |
   +-----+-------+-------------+-----------------------+-------------------+-------------+-----------+
   | Y   |  ?    | **EF497**   | Beheizung: Überw.     | Energy Source     | Household   | Tab.8_    |
   |     |       |             | verwendete Energieart |                   | (DU)        |           |
   +-----+-------+-------------+-----------------------+-------------------+-------------+-----------+

.. note::

   (1) bench

.. _Tab.2:

.. table:: Table 2: parameter - **EF1:** Bundesland [Min/Max (Valid) 1/16]

   +-------+--------------------+------------+-----------+-----------+
   | Value | Value Label        | Frequency  | Total [%] | Valid [%] |
   +=======+====================+============+===========+===========+
   | 1     | Schleswig-Holstein | 17360      | 3.5       | 3.5       |
   +-------+--------------------+------------+-----------+-----------+
   | 2     | Hamburg            | 9982       | 2.0       | 2.0       |
   +-------+--------------------+------------+-----------+-----------+
   | ...   |                    |            |           |           |
   +-------+--------------------+------------+-----------+-----------+
   | 15    | Sachsen-Anhalt     | 14922      | 3.0       | 3.0       |
   +-------+--------------------+------------+-----------+-----------+
   | 16    | Thüringen          | 14310      | 2.9       | 2.9       |
   +-------+--------------------+------------+-----------+-----------+
   |       | **Valid Total**    | **489630** | **100**   | **100**   |
   +-------+--------------------+------------+-----------+-----------+
   |       | **Total**          | **489630** | **100**   |           |
   +-------+--------------------+------------+-----------+-----------+

.. _Tab.3:

.. table:: Table 3: parameter - **EF494** Baujahr der Wohnung [Min/Max (Valid) 1/99]

    +-------+---------------------------+------------+-----------+-----------+
    | Value | Value Label               | Frequency  | Total [%] | Valid [%] |
    +=======+===========================+============+===========+===========+
    | 1     | Vor 1919                  | 65936      | 13.5      | 14.1      |
    +-------+---------------------------+------------+-----------+-----------+
    | 2     | 1919 bis 1948             | 57754      | 11.8      | 12.3      |
    +-------+---------------------------+------------+-----------+-----------+
    | 3     | 1949 bis 1978             | 204168     | 41.7      | 43.5      |
    +-------+---------------------------+------------+-----------+-----------+
    | 4     | 1979 bis 1986             | 49499      | 10.1      | 10.6      |
    +-------+---------------------------+------------+-----------+-----------+
    | 5     | 1987 bis 1990             | 14587      | 3.0       | 3.1       |
    +-------+---------------------------+------------+-----------+-----------+
    | 6     | 1991 bis 2000             | 45085      | 9.2       | 9.6       |
    +-------+---------------------------+------------+-----------+-----------+
    | 7     | 2001 bis 2004             | 15957      | 3.3       | 3.4       |
    +-------+---------------------------+------------+-----------+-----------+
    | 8     | 2005 bis 2008             | 12407      | 2.5       | 2.6       |
    +-------+---------------------------+------------+-----------+-----------+
    | 9     | 2009 oder später          | 2211       | 0.5       | 0.5       |
    +-------+---------------------------+------------+-----------+-----------+
    | 99    | Keine Angabe              | 1357       | 0.3       | 0.3       |
    +-------+---------------------------+------------+-----------+-----------+
    |       | **Valid Total**           | **468961** | **95.8**  | **100**   |
    +-------+---------------------------+------------+-----------+-----------+
    | (M-1) | Entfällt                  | 5964       | 1.2       |           |
    |       | (Gemeinschaftsunterkunft) |            |           |           |
    +-------+---------------------------+------------+-----------+-----------+
    | (M-5) | Entfällt (Jahresüberhang; | 14705      | 3         |           |
    |       | Wohnheim                  |            |           |           |
    |       | ohne eigene               |            |           |           |
    |       | Haushaltsführung          |            |           |           |
    |       | der Bewohner)             |            |           |           |
    +-------+---------------------------+------------+-----------+-----------+
    |       | **Total**                 | **489630** | **100**   |           |
    +-------+---------------------------+------------+-----------+-----------+

.. _Tab.4:

.. table:: Table 4: parameter - **EF496** Überw. Beheizung der Wohnung [Min/Max (Valid) 1/9]

   +-------+---------------------------+------------+-----------+-----------+
   | Value | Value Label               | Frequency  | Total [%] | Valid [%] |
   +=======+===========================+============+===========+===========+
   | 1     | Fernheizung               | 54438      | 11.1      | 11.6      |
   +-------+---------------------------+------------+-----------+-----------+
   | 2     | Blockheizung,             | 345572     | 70.6      | 73.7      |
   |       | Zentralheizung            |            |           |           |
   +-------+---------------------------+------------+-----------+-----------+
   | 3     | Etagenheizung             | 36383      | 7.4       | 7.8       |
   +-------+---------------------------+------------+-----------+-----------+
   | 4     | Einzel- oder Mehrraumöfen | 31211      | 6.4       | 6.7       |
   |       | (auch Elektrospeicher)    |            |           |           |
   +-------+---------------------------+------------+-----------+-----------+
   | 9     | Keine Angabe              | 1357       | 0.3       | 0.3       |
   +-------+---------------------------+------------+-----------+-----------+
   |       | **Valid Total**           | **468961** | **95.8**  | **100**   |
   +-------+---------------------------+------------+-----------+-----------+
   | (M-1) | Entfällt                  | 5964       | 1.2       |           |
   |       | (Gemeinschaftsunterkunft) |            |           |           |
   +-------+---------------------------+------------+-----------+-----------+
   | (M-5) | Entfällt (Jahresüberhang; | 14705      | 3         |           |
   |       | Wohnheim                  |            |           |           |
   |       | ohne eigene               |            |           |           |
   |       | Haushaltsführung          |            |           |           |
   |       | der Bewohner)             |            |           |           |
   +-------+---------------------------+------------+-----------+-----------+
   |       | **Total**                 | **489630** | **100**   |           |
   +-------+---------------------------+------------+-----------+-----------+

.. _Tab.5a:

.. table:: Table 5a: parameter - **EF501** Warme Nebenkosten: Höhe [Min/Max (Valid) 1/9999]

    +------------+-----+
    | Perzentile |     |
    +============+=====+
    | 10         | 45  |
    +------------+-----+
    | 20         | 55  |
    +------------+-----+
    | 30         | 63  |
    +------------+-----+
    | 40         | 71  |
    +------------+-----+
    | 50         | 80  |
    +------------+-----+
    | 60         | 90  |
    +------------+-----+
    | 70         | 100 |
    +------------+-----+
    | 80         | 116 |
    +------------+-----+
    | 90         | 150 |
    +------------+-----+

.. _Tab.5b:

.. table:: Table 5b: parameter - **EF501** Warme Nebenkosten: Höhe [Min/Max (Valid) 1/9999]

    +-------+-----------------------------+------------+-----------+-----------+
    | Value | Value Label                 | Frequency  | Total [%] | Valid [%] |
    +=======+=============================+============+===========+===========+
    |       | **Valid Total**             | **173078** | **35.3**  | **100**   |
    +-------+-----------------------------+------------+-----------+-----------+
    | (M-1) | Entfällt                    | 5964       | 1.2       |           |
    |       | (Gemeinschaftsunterkunft)   |            |           |           |
    +-------+-----------------------------+------------+-----------+-----------+
    |       | **Total**                   | **489630** | **100**   |           |
    +-------+-----------------------------+------------+-----------+-----------+
    | (M-5) | Entfällt (Jahresüberhang;   | 310588     | 63.4      |           |
    |       | Eigentümer; Untermieter;    |            |           |           |
    |       | keine warmen Nebenkosten in |            |           |           |
    |       | der monatlichen Mietzahlung |            |           |           |
    |       | enthalten)                  |            |           |           |
    +-------+-----------------------------+------------+-----------+-----------+

.. _Tab.6:

.. table:: Table 6: parameter - **EF563** Gemeindegrößenklasse [Min/Max (Valid) 1/9]

    +-------+-------------------------+------------+-----------+-----------+
    | Value | Value Label             | Frequency  | Total [%] | Valid [%] |
    +=======+=========================+============+===========+===========+
    | 1     | unter 5000 Einwohner    | 72105      | 14.7      | 14.7      |
    +-------+-------------------------+------------+-----------+-----------+
    | 2     | 5000 bis unter 20000    | 97185      | 19.8      | 19.8      |
    |       | Einwohner               |            |           |           |
    +-------+-------------------------+------------+-----------+-----------+
    | 3     | 20000 bis unter 100000  | 115998     | 23.7      | 23.7      |
    |       | Einwohner               |            |           |           |
    +-------+-------------------------+------------+-----------+-----------+
    | 4     | 100000 bis unter 500000 | 65802      | 13.4      | 13.4      |
    |       | Einwohner               |            |           |           |
    +-------+-------------------------+------------+-----------+-----------+
    | 5     | 500000 Einwohner        | 64589      | 13.2      | 13.2      |
    |       | und mehr                |            |           |           |
    +-------+-------------------------+------------+-----------+-----------+
    | 6     | unter 20000 Einwohner   | 36063      | 7.4       | 7.4       |
    |       | (Nordrhein-Westfalen,   |            |           |           |
    |       | Hessen, Sachsen-Anhalt) |            |           |           |
    +-------+-------------------------+------------+-----------+-----------+
    | 7     | 20000 bis unter 500000  | 25163      | 5.1       | 5.1       |
    |       | Einwohner               |            |           |           |
    |       | (Saarland, Brandenburg, |            |           |           |
    |       | Mecklenburg-Vorpommern, |            |           |           |
    |       | Sachsen, Thüringen)     |            |           |           |
    +-------+-------------------------+------------+-----------+-----------+
    | 8     | Bremen                  | 3569       | 0.7       | 0.7       |
    |       | (einziger Schlüssel)    |            |           |           |
    +-------+-------------------------+------------+-----------+-----------+
    | 9     | Berlin-Ost              | 9156       | 1.9       | 1.9       |
    |       | (einziger Schlüssel)    |            |           |           |
    +-------+-------------------------+------------+-----------+-----------+
    |       | **Valid Total**         | **489630** | **100**   | **100**   |
    +-------+-------------------------+------------+-----------+-----------+
    |       | **Total**               | **489630** | **100**   |           |
    +-------+-------------------------+------------+-----------+-----------+

.. _Tab.7:

.. table:: Table 7: parameter - **EF570** Gebäudegrößenklasse [Min/Max (Valid) 1/7]

    +-------+------------------------------------+------------+-----------+-----------+
    | Value | Value Label                        | Frequency  | Total [%] | Valid [%] |
    +=======+====================================+============+===========+===========+
    | 1     | Kleingebäude (Grundauswahl)        | 253286     | 51.7      | 51.7      |
    +-------+------------------------------------+------------+-----------+-----------+
    | 2     | mittelgroße Gebäude (Grundauswahl) | 88765      | 18.1      | 18.1      |
    +-------+------------------------------------+------------+-----------+-----------+
    | 3     | Großgebäude (Grundauswahl)         | 54359      | 11.1      | 11.1      |
    +-------+------------------------------------+------------+-----------+-----------+
    | 4     | Gemeinschafts-/Anstaltsunterkünfte | 3573       | 0.7       | 0.7       |
    |       | (Grundauswahl)                     |            |           |           |
    +-------+------------------------------------+------------+-----------+-----------+
    | 6     | Neubauten (Neubauauswahl)          | 87931      | 18.0      | 18.0      |
    +-------+------------------------------------+------------+-----------+-----------+
    | 7     | Neubauauswahl aktuelles Jahr       | 1716       | 0.4       | 0.4       |
    +-------+------------------------------------+------------+-----------+-----------+
    |       | **Valid Total**                    | **489630** | **100**   | **100**   |
    +-------+------------------------------------+------------+-----------+-----------+
    |       | **Total**                          | **489630** | **100**   |           |
    +-------+------------------------------------+------------+-----------+-----------+

.. _Tab.8:

.. table:: Table 8: parameter - **EF497** Beheizung: Überw. verwendete Energieart [Min/Max (Valid) 1/99]

    +-------+---------------------------+------------+-----------+------------+-----+----------------+
    | Value | Value Label               | Frequency  | Total [%] | Valid  [%] | fp  | CO2_eq [kg/TJ] |
    +=======+===========================+============+===========+============+=====+================+
    | 1     | Fernwärme                 | 53991      | 11.0      | 11.5       | 1.3 | 74.2583e3 (a)  |
    |       | (bei Fernheizung)         |            |           |            |     | 71.4750e3 (b)  |
    +-------+---------------------------+------------+-----------+------------+-----+----------------+
    | 2     | Gas                       | 229324     | 46.8      | 48.9       | 1.1 | 85.9456e3 (c)  |
    +-------+---------------------------+------------+-----------+------------+-----+----------------+
    | 3     | Elektrizität, Strom       | 16897      | 3.5       | 3.6        | 2.8 | 163.235e3 (d)  |
    |       | (ohne Wärmepumpe)         |            |           |            |     |                |
    +-------+---------------------------+------------+-----------+------------+-----+----------------+
    | 4     | Heizöl                    | 134430     | 27.5      | 28.7       | 1.1 | 104.010e3 (e)  |
    +-------+---------------------------+------------+-----------+------------+-----+----------------+
    | 5     | Briketts, Braunkohle      | 2332       | 0.5       | 0.5        | 1.2 | 188.605e3 (f)  |
    +-------+---------------------------+------------+-----------+------------+-----+----------------+
    | 6     | Koks, Steinkohle          | 957        | 0.2       | 0.2        | 1.1 | 188.605e3 (f)  |
    +-------+---------------------------+------------+-----------+------------+-----+----------------+
    | 7     | Holz, Holzpellets         | 19971      | 4.1       | 4.3        | 1.2 | 14.8918e3 (g)  |
    +-------+---------------------------+------------+-----------+------------+-----+----------------+
    | 8     | Biomasse (außer Holz),    | 361        | 0.1       | 0.1        | 1.5 | 27.3013e3 (h)  |
    |       | Biogas                    |            |           |            |     |                |
    +-------+---------------------------+------------+-----------+------------+-----+----------------+
    | 9     | Sonnenenergie             | 385        | 0.1       | 0.1        | 1.0 | 6.90949e3 (i)  |
    |       | (Solarkollektoren)        |            |           |            |     |                |
    +-------+---------------------------+------------+-----------+------------+-----+----------------+
    | 10    | Erd- und andere           | 4802       | 1.0       | 1.0        | 1.0 | 52.6688e3 (j)  |
    |       | Umweltwärme,              |            |           |            |     | 60.6866e3 (j)  |
    |       | Abluftwärme               |            |           |            |     | 69.3211e3 (j)  |
    |       | (Wärmepumpen,             |            |           |            |     | 44.2813e3 (k)  |
    |       | -tauscher)                |            |           |            |     | 51.1731e3 (k)  |
    |       |                           |            |           |            |     | 57.8865e3 (k)  |
    +-------+---------------------------+------------+-----------+------------+-----+----------------+
    | 99    | Keine Angabe              | 5511       | 1.1       | 1.2        | /   |                |
    +-------+---------------------------+------------+-----------+------------+-----+----------------+
    |       | **Valid Total**           | **468961** | **95.8**  | **100**    |     |                |
    +-------+---------------------------+------------+-----------+------------+-----+----------------+
    | (M-1) | Entfällt                  | 5964       | 1.2       |            | /   |                |
    |       | (Gemeinschaftsunterkunft) |            |           |            |     |                |
    +-------+---------------------------+------------+-----------+------------+-----+----------------+
    | (M-5) | Entfällt (Jahresüberhang; | 14705      | 3         |            | /   |                |
    |       | Wohnheim ohne             |            |           |            |     |                |
    |       | eigene Haushaltsführung   |            |           |            |     |                |
    |       | der Bewohner)             |            |           |            |     |                |
    +-------+---------------------------+------------+-----------+------------+-----+----------------+
    |       | **Total**                 | **489630** | **100**   |            |     |                |
    +-------+---------------------------+------------+-----------+------------+-----+----------------+

.. note::

    (fp) is the primary energy factor attributed to the energy fule, see
    Tab.14_.

    (CO2_eq) Equivalent CO2 emissions for the corrsponding fule. Source: GEMIS
    - Globales Emissions-Modell integrierter Systeme, version: 4.93

    (a) Mix of peaking boilers for district heating delivery in Germany in year
        2010. All data estimated from [AGFW]_ (2007).

    (b) Mix of district heat from cogeneration (CHP) plats, and peaking
        boilers, as well as industrial waste heat, for district heating
        delivery in Germany in year 2010. All data estimated from [AGFW]_ (2007).
        Here with energy-based allocation between cogenerated electricity and
        cogenerated heat.

    (c) Residential central heating with liquid gas, atmospheric burner, inc.
        aux. electricity and heat distribution in the building.
        All data from [OEKO]_ (1994), emission
        data updated from [GEMIS]_ (1989ff), NOx emissions adjusted to reflect
        §7 of the 1. BlmSchV

    (d) Mix of electricity generation in Germany, data from [BMWi.a]_ (2011),
        updated with energy statistics based on [AGEEStat]_ (2014), [BMWi.b]_
        (2014) and [BMWi.c]_ (2014)

    (e) Residential central heating with light oil, atmospheric burner, inc.
        tank, aux. electricity and heat distribution in the building based on
        [OEKO.b]_, emission data based on [IVD]_ 2000.

    (f) Central heating system for hardcoal briquettes in Germany, inc.
        auxiliary electricity and heat distribution in the building. All data
        from [OEKO.b]_ (1994), emission data updated from [IVD]_ (2000).

    (g) Small-scale wood pellet central heating system. incl. auxiliary
        electricity. Emissions from [EVA]_ (2002) and [Nussbaumer]_ (2002).
        PAH- and PCDD/F as well as CH4/N2O from [IVD]_ (2000); cost data and
        efficiency from [Fichtner]_. Changes of data fro 2010 from
        [OEKO.c]_ (2003).

    (h) Mix of district heat from internal combustion engine (ICE) cogeneration
        plant using biogas (mix maize/manure) with energy-based allocation for
        cogenerated heat, and gas peaking boiler for small district heating in
        a model mix area for single- and multi-family buildings (SFB+MFB).

    (i) Solar thermal collector for water heating typical for Germany, incl.
        storage (150 l) and balance of system. Energy and material data are
        from [BMU]_ (2012).

    (j) Mono-energetic electric heat pump, heat source is ambient air, with
        direct electric heating for cold days. Electricity from power plant mix
        (mix case). All data from [OEKO.b]_ (1994), the coefficient of
        performance COP is based on a 7.5% increase of the 2000 data.

    (k) Monovalent electric heat-pump, heat source is ground water. Electricity
        from power plant mix. All data from [OEKO.b]_ (1994).


.. _Tab.9:

.. table:: Table 9: parameter - **EF809** Typ der Lebensform (Konzept der Lebensformen) [Min/Max (Valid) 1/8]

    +-------+---------------------------------------------+------------+-----------+-----------+
    | Value | Value Label                                 | Frequency  | Total [%] | Valid [%] |
    +=======+=============================================+============+===========+===========+
    | 1     | F1 Ehepaar mit ledigen Kindern              | 182311     | 37.2      | 37.7      |
    +-------+---------------------------------------------+------------+-----------+-----------+
    | 2     | F2 Nichteheliche Lebensgemeinschaft         | 16319      | 3.3       | 3.4       |
    |       | mit ledigen Kindern                         |            |           |           |
    +-------+---------------------------------------------+------------+-----------+-----------+
    | 3     | F3 Gleichgeschlechtliche                    | 103        | 0.0       | 0.0       |
    |       | Lebensgemeinschaft mit ledigen Kindern      |            |           |           |
    +-------+---------------------------------------------+------------+-----------+-----------+
    | 4     | F4 Alleinerziehende mit ledigen Kindern     | 38118      | 7.8       | 7.9       |
    +-------+---------------------------------------------+------------+-----------+-----------+
    | 5     | F5 Ehepaar ohne ledige Kinder               | 121686     | 24.9      | 25.2      |
    +-------+---------------------------------------------+------------+-----------+-----------+
    | 6     | F6 Nichteheliche Lebensgemeinschaft         | 20384      | 4.2       | 4.2       |
    |       | ohne ledige Kinder                          |            |           |           |
    +-------+---------------------------------------------+------------+-----------+-----------+
    | 7     | F7 Gleichgeschlechtliche Lebensgemeinschaft | 646        | 0.1       | 0.1       |
    |       | ohne ledige Kinder                          |            |           |           |
    +-------+---------------------------------------------+------------+-----------+-----------+
    | 8     | F8 Alleinstehende                           | 104207     | 21.3      | 21.5      |
    +-------+---------------------------------------------+------------+-----------+-----------+
    |       | **Valid Total**                             | **483774** | **98.8**  | **100**   |
    +-------+---------------------------------------------+------------+-----------+-----------+
    | (M-1) | Entfällt (Gemeinschaftsunterkunft)          | 5856       | 1.2       |           |
    +-------+---------------------------------------------+------------+-----------+-----------+
    |       | **Total**                                   | **489630** | **100**   |           |
    +-------+---------------------------------------------+------------+-----------+-----------+

.. _Tab.10:

.. table:: Table 10: parameter - **EF491** Eigentum der Mietwohnung [Min/Max (Valid) 1 / 9]

    +-------+--------------------------------------------+------------+-----------+-----------+
    | Value | Value Label                                | Frequency  | Total [%] | Valid [%] |
    +=======+============================================+============+===========+===========+
    | 1     | Eigentümer des Gebäudes                    | 194045     | 39.6      | 41.4      |
    +-------+--------------------------------------------+------------+-----------+-----------+
    | 2     | Eigentümer der Wohnung                     | 49735      | 10.2      | 10.6      |
    +-------+--------------------------------------------+------------+-----------+-----------+
    | 3     | Hauptmieter                                | 216924     | 44.3      | 46.3      |
    +-------+--------------------------------------------+------------+-----------+-----------+
    | 4     | Untermieter                                | 6900       | 1.4       | 1.5       |
    +-------+--------------------------------------------+------------+-----------+-----------+
    | 9     | Keine Angabe                               | 1357       | 0.3       | 0.3       |
    +-------+--------------------------------------------+------------+-----------+-----------+
    |       | **Valid Total**                            | **468961** | **95.8**  | **100**   |
    +-------+--------------------------------------------+------------+-----------+-----------+
    | (M-1) | Entfällt (Gemeinschaftsunterkunft)         | 5964       | 1.2       |           |
    +-------+--------------------------------------------+------------+-----------+-----------+
    | (M-5) | Entfällt (Jahresüberhang; Wohnheim         | 14705      | 3         |           |
    |       | ohne eigene Haushaltsführung der Bewohner) |            |           |           |
    +-------+--------------------------------------------+------------+-----------+-----------+
    |       | **Total**                                  | **489630** | **100**   |           |
    +-------+--------------------------------------------+------------+-----------+-----------+

.. _Tab.11:

.. table:: Table 11: parameter - **EF131** - Normale Arbeitszeit (je Woche): Stunden [Min/Max (Valid) 1 / 98]

    +-------+-------------------------+------------+-----------+-----------+
    | Value | Value Label             | Frequency  | Total [%] | Valid [%] |
    +=======+=========================+============+===========+===========+
    | 1     | 1 Stunde                | 226        | 0.0       | 0.1       |
    +-------+-------------------------+------------+-----------+-----------+
    | ...   |                         |            |           |           |
    +-------+-------------------------+------------+-----------+-----------+
    | 98    | 98 oder mehr Stunden    | 66         | 0.0       | 0.0       |
    +-------+-------------------------+------------+-----------+-----------+
    |       | Valid Total             | 231122     | 47.2      | 100       |
    +-------+-------------------------+------------+-----------+-----------+
    | (M-3) | Entfällt                | 193882     | 39.6      |           |
    |       | (Nichterwerbstätiger)   |            |           |           |
    +-------+-------------------------+------------+-----------+-----------+
    | (M-2) | Entfällt                | 64626      | 13.2      |           |
    |       | (Person unter 15 Jahre) |            |           |           |
    +-------+-------------------------+------------+-----------+-----------+
    |       | **Total**               | **489630** | **100**   |           |
    +-------+-------------------------+------------+-----------+-----------+

.. _Tab.12:

.. table:: Table 12: parameter - **EF171** - Heimarbeit [Min/Max (Valid) 1 / 9]

    +-------+-----------------------------+------------+-----------+-----------+
    | Value | Value Label                 | Frequency  | Total [%] | Valid [%] |
    +=======+=============================+============+===========+===========+
    | 1     | Hauptsächlich (mindestens   | 8443       | 1.7       | 3.7       |
    |       | die Hälfte der Arbeitszeit) |            |           |           |
    +-------+-----------------------------+------------+-----------+-----------+
    | 2     | Manchmal                    | 22228      | 4.5       | 9.6       |
    +-------+-----------------------------+------------+-----------+-----------+
    | 8     | Nie                         | 200266     | 40.9      | 86.6      |
    +-------+-----------------------------+------------+-----------+-----------+
    | 9     | Keine Angabe                | 185        | 0.0       | 0.1       |
    |       | (nur Erwerbstätige)         |            |           |           |
    +-------+-----------------------------+------------+-----------+-----------+
    |       | Valid Total                 | 231122     | 47.2      | 100       |
    +-------+-----------------------------+------------+-----------+-----------+
    | (M-3) | Entfällt                    |  193882    | 39.6      |           |
    |       | (Nichterwerbstätiger)       |            |           |           |
    +-------+-----------------------------+------------+-----------+-----------+
    | (M-2) | Entfällt                    | 64626      | 13.2      |           |
    |       | (Person unter 15 Jahre)     |            |           |           |
    +-------+-----------------------------+------------+-----------+-----------+
    |       | **Total**                   | **489630** | **100**   |           |
    +-------+-----------------------------+------------+-----------+-----------+


Zensus 2011 (Benchmark Tables):
----------------------------------------------

.. _Tab.13:

.. table:: Table 13: Zenzus benchmarks

    +--------+---------------------+------------------------+-------------+-----------+
    | bench. | Variable ID         | Name                   | Aggregation | Link for  |
    |        |                     |                        | Level       |           |
    +========+=====================+========================+=============+===========+
    | b.1    | **ALTER_KURZ**      | Alter                  | Individual  |           |
    |        |                     | (5 Altersklassen)      |             |           |
    +--------+---------------------+------------------------+-------------+-----------+
    | b.2    | **FAMSTND_KURZ**    | Familienstand          | Individual  |           |
    +--------+---------------------+------------------------+-------------+-----------+
    | b.3    | **GESCHLECHT**      | Geschlecht             | Individual  |           |
    +--------+---------------------+------------------------+-------------+-----------+
    | b.4    | **HHGROESS_KLASS**  | Größe des privaten     | Individual  | Household |
    |        |                     | Haushalts              |             | (DU)      |
    +--------+---------------------+------------------------+-------------+-----------+
    | b.5    | **MIGRATION_KURZ**  | Migrationshintergrund  | Individual  |           |
    +--------+---------------------+------------------------+-------------+-----------+
    | b.6    | **WOHNFLAECHE_10S** | Fläche der Wohnung     | Household   |           |
    |        |                     | (10 m²-Intervalle)     | (DU)        |           |
    +--------+---------------------+------------------------+-------------+-----------+
    | b.8    | **HHTYP_FAM**       | Familien nach Typ      | Household   |           |
    |        |                     | des privaten Haushalts | (DU)        |           |
    |        |                     | (nach Familien)        |             |           |
    +--------+---------------------+------------------------+-------------+-----------+
    | *      | **HHGROESS_KLASS**  | Größe des privaten     | Household   |           |
    |        |                     | Haushalts              | (DU)        |           |
    +--------+---------------------+------------------------+-------------+-----------+
    | b.9    | **ZAHLWOHNGN_HHG**  | Zahl der Wohnungen     | Household   | Building  |
    |        |                     | im Gebäude             | (DU)        |           |
    +--------+---------------------+------------------------+-------------+-----------+
    | b.10   | **BAUJAHR_MZ**      | Baujahr                | Building    |           |
    |        |                     | (Mikrozensus-Klassen)  |             |           |
    +--------+---------------------+------------------------+-------------+-----------+
    | b.11   | **HEIZTYP**         | Heizungsart            | Building    |           |
    +--------+---------------------+------------------------+-------------+-----------+
    | b.12   | **GEBAEUDEART_SYS** | Art des Gebäudes       | Building    |           |
    +--------+---------------------+------------------------+-------------+-----------+
    | b.14   | **GEBTYPBAUWEISE**  | Gebäudetyp-Bauweise    | Building    |           |
    +--------+---------------------+------------------------+-------------+-----------+
    | *      | **ZAHLWOHNGN_HHG**  | Zahl der Wohnungen     | Building    |           |
    |        |                     | im Gebäude             |             |           |
    +--------+---------------------+------------------------+-------------+-----------+

Primary Energy Demand Factors
----------------------------------------------

Primary energy factors for Germany.

.. _Tab.14:

.. table:: Table 14: Primary energy factors for Germany. Source: DIN V 18599-1:2011-12

    +----------------------------------------+----------------+
    | Fuel                                   | Primary energy |
    |                                        | factor (fp)    |
    +===================+====================+================+
    | Fossile Fules     | Heating oil        | 1.1            |
    |                   +--------------------+----------------+
    |                   | Natural gas        | 1.1            |
    |                   +--------------------+----------------+
    |                   | LPG                | 1.1            |
    |                   +--------------------+----------------+
    |                   | Black Coal         | 1.1            |
    |                   +--------------------+----------------+
    |                   | Brown Coal         | 1.2            |
    +-------------------+--------------------+----------------+
    | Biogenic Fules    | Biogas             | 1.5            |
    |                   +--------------------+----------------+
    |                   | Bio-oil            | 1.5            |
    |                   +--------------------+----------------+
    |                   | Wood               | 1.2            |
    +-------------------+--------------------+----------------+
    | District Heating  | Fossil fuel        | 0.7            |
    | (CHP)             +--------------------+----------------+
    |                   | Renewable          | 0.7            |
    +-------------------+--------------------+----------------+
    | Disctric Heating  | Fossil fuel        | 1.3            |
    | (Centralized heat +--------------------+----------------+
    | supply Station)   | Renewable          | 1.3            |
    +-------------------+--------------------+----------------+
    | Electricity       | Electricity Matrix | 2.8            |
    +-------------------+--------------------+----------------+
    | Enviromental      | Solar              | 1.0            |
    | Friendly Energy   +--------------------+----------------+
    |                   | Geothermal         | 1.0            |
    +-------------------+--------------------+----------------+

The primary energy factors for Germany are used as input to the heat demand
model.

Code
============

Regression Analysis on Energy Source
----------------------------------------------

Load and prepare the data
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

We import the python libraries used in this analysis

.. code-block:: python
   :linenos:

   import pandas as pd
   import statsmodels.api as sm
   import numpy as np

We load the raw data from the Microcensus with the variables we identify as
relevant for the analysis, see metadata for the variable codes.

.. code-block:: python
   :linenos:

   raw_data = pd.read_csv(
       "data/suf2010.csv",
       usecols=["EF1",    # State
                "EF494",  # Construction Year
                "EF496",  # Heating typ
                "EF501",  # Heat Expenditure
                "EF563",  # Small Area size
                "EF570",  # Building size
                "EF497"], # Energy Source
       na_values=["(M)-1", "(M)-5", -1, -5])

Define nan values on the dataset.

.. code-block:: python
   :linenos:

   raw_data["EF494"] = raw_data.EF494.replace(99, np.nan)
   raw_data["EF497"] = raw_data.EF497.replace(99, np.nan)
   raw_data["EF496"] = raw_data.EF496.replace(9, np.nan)

And delete observations containing unknown observations/nans from the dataset.

.. code-block:: python
   :linenos:

   raw_data.dropna(inplace=True)

Define the analysis fuction
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Define the function

.. code-block:: python
   :linenos:

    def get_cols(reg_data):
        cols = reg_data.columns
        cols = cols.delete(1)
        cols = cols[:reg_data.shape[1] -1]
        return cols

.. code-block:: python
   :linenos:

    def prepare_data(val,
                     explanatory_var_cat=['EF1', 'EF494', 'EF496', 'EF563', 'EF570'],
                     dependent_var='EF497'
                    ):
        reg_data = raw_data[['EF501', 'EF497']]
        reg_data['intercept'] = 1.0
        A = [a for a in range(1, val[1] +1)]
        A.remove(val[0])
        reg_data[dependent_var] = reg_data[dependent_var].replace(A, 0)
        reg_data[dependent_var] = reg_data[dependent_var].replace(val[0], 1)
        if val[0] in [1,5,6,7,8,9,10]:
            if val[0] == 1:                                                      ## (1) Fernwärme
                dummy_ef496 = raw_data.EF496.replace([2,3,4], 0)
                dummy_ef496 = raw_data.EF496.replace(1, 1)                        # (A) Fernheizung
                reg_data = reg_data.join(dummy_ef496)
                dummy_ef1   = pd.get_dummies(raw_data['EF1'],   prefix='EF1'  )   # (B) State
                reg_data = reg_data.join(dummy_ef1.ix[:, 'EF1_2':])
                dummy_ef563 = pd.get_dummies(raw_data['EF563'], prefix='EF563')   # (D) Small Area size
                reg_data = reg_data.join(dummy_ef563.ix[:, 'EF563_2':])
            elif val[0] == 5:                                                    ## (5) Heizöl
                dummy_ef496 = raw_data.EF496.replace([1,3,4], 0)
                dummy_ef496 = raw_data.EF496.replace(2, 1)                        # (A) Blockheizung, Zentralheizung
                reg_data = reg_data.join(dummy_ef496)
                dummy_ef1   = pd.get_dummies(raw_data['EF1'],   prefix='EF1'  )   # (B) State
                reg_data = reg_data.join(dummy_ef1.ix[:, 'EF1_2':])
                dummy_ef494 = pd.get_dummies(raw_data['EF494'], prefix='EF494')   # (C) Construction Year
                reg_data = reg_data.join(dummy_ef494.ix[:, 'EF494_2.0':])
                dummy_ef570 = pd.get_dummies(raw_data['EF570'], prefix='EF570')   # (E) Building size
                reg_data = reg_data.join(dummy_ef570.ix[:, 'EF570_2':])
            elif val[0] == 6:                                                    ## (6) Koks, Steinkohle
                dummy_ef496 = pd.get_dummies(raw_data['EF496'], prefix='EF496')   # (A) Heating typ - All
                reg_data = reg_data.join(dummy_ef496.ix[:, 'EF496_2.0':])
                dummy_ef1   = pd.get_dummies(raw_data['EF1'],   prefix='EF1'  )   # (B) State
                reg_data = reg_data.join(dummy_ef1.ix[:, 'EF1_2':])
                dummy_ef563 = pd.get_dummies(raw_data['EF563'], prefix='EF563')   # (D) Small Area size
                reg_data = reg_data.join(dummy_ef563.ix[:, 'EF563_2':])
                dummy_ef570 = pd.get_dummies(raw_data['EF570'], prefix='EF570')   # (E) Building size
                reg_data = reg_data.join(dummy_ef570.ix[:, 'EF570_2':])
            elif val[0] == 7:                                                    ## (7) Holz, Holzpellets
                dummy_ef496 = raw_data.EF496.replace([1,3,4], 0)
                dummy_ef496 = raw_data.EF496.replace(2, 1)                        # (A) Blockheizung, Zentralheizung
                reg_data = reg_data.join(dummy_ef496)
                dummy_ef1   = pd.get_dummies(raw_data['EF1'],   prefix='EF1'  )   # (B) State
                reg_data = reg_data.join(dummy_ef1.ix[:, 'EF1_2':])
                dummy_ef494 = pd.get_dummies(raw_data['EF494'], prefix='EF494')   # (C) Construction Year
                reg_data = reg_data.join(dummy_ef494.ix[:, 'EF494_2.0':])
                dummy_ef563 = pd.get_dummies(raw_data['EF563'], prefix='EF563')   # (D) Small Area size
                reg_data = reg_data.join(dummy_ef563.ix[:, 'EF563_2':])
            elif val[0] == 8:                                                    ## (8) Biomasse (außer Holz), Biogas
                dummy_ef496 = raw_data.EF496.replace([1,3,4], 0)
                dummy_ef496 = raw_data.EF496.replace(2, 1)                        # (A) Blockheizung, Zentralheizung
                reg_data = reg_data.join(dummy_ef496)
                dummy_ef494 = pd.get_dummies(raw_data['EF494'], prefix='EF494')   # (C) Construction Year
                reg_data = reg_data.join(dummy_ef494.ix[:, 'EF494_2.0':])
            elif val[0] == 9:                                                    ## (9) Sonnenenergie (Solarkollektoren)
                dummy_ef496 = pd.get_dummies(raw_data['EF496'], prefix='EF496')   # (A) Heating typ - All
                reg_data = reg_data.join(dummy_ef496.ix[:, 'EF496_2.0':])
                dummy_ef1   = pd.get_dummies(raw_data['EF1'],   prefix='EF1'  )   # (B) State
                reg_data = reg_data.join(dummy_ef1.ix[:, 'EF1_2':])
                dummy_ef494 = pd.get_dummies(raw_data['EF494'], prefix='EF494')   # (C) Construction Year
                reg_data = reg_data.join(dummy_ef494.ix[:, 'EF494_2.0':])
            elif val[0] == 10:                                                   ## (10) Wärmepumpen
                dummy_ef496 = raw_data.EF496.replace([1,2,3], 0)
                dummy_ef496 = raw_data.EF496.replace(4, 1)                        # (A) Einzel- oder Mehrraumöfen (auch Elektrospeicher)
                reg_data = reg_data.join(dummy_ef496)
                dummy_ef1   = pd.get_dummies(raw_data['EF1'],   prefix='EF1'  )   # (B) State
                reg_data = reg_data.join(dummy_ef1.ix[:, 'EF1_2':])
                dummy_ef494 = pd.get_dummies(raw_data['EF494'], prefix='EF494')   # (C) Construction Year
                reg_data = reg_data.join(dummy_ef494.ix[:, 'EF494_2.0':])
                dummy_ef563 = pd.get_dummies(raw_data['EF563'], prefix='EF563')   # (D) Small Area size
                reg_data = reg_data.join(dummy_ef563.ix[:, 'EF563_2':])
                dummy_ef570 = pd.get_dummies(raw_data['EF570'], prefix='EF570')   # (E) Building size
                reg_data = reg_data.join(dummy_ef570.ix[:, 'EF570_2':])
        else:
            dummy_ef496 = pd.get_dummies(raw_data['EF496'], prefix='EF496')       # (A) Heating typ - All
            reg_data = reg_data.join(dummy_ef496.ix[:, 'EF496_2.0':])
            dummy_ef1   = pd.get_dummies(raw_data['EF1'],   prefix='EF1'  )       # (B) State
            reg_data = reg_data.join(dummy_ef1.ix[:, 'EF1_2':])
            dummy_ef494 = pd.get_dummies(raw_data['EF494'], prefix='EF494')       # (C) Construction Year
            reg_data = reg_data.join(dummy_ef494.ix[:, 'EF494_2.0':])
            dummy_ef563 = pd.get_dummies(raw_data['EF563'], prefix='EF563')       # (D) Small Area size
            reg_data = reg_data.join(dummy_ef563.ix[:, 'EF563_2':])
            dummy_ef570 = pd.get_dummies(raw_data['EF570'], prefix='EF570')       # (E) Building size
            reg_data = reg_data.join(dummy_ef570.ix[:, 'EF570_2':])
        return reg_data

.. code-block:: python
   :linenos:

   def make_logit(reg_data, dependent_var='EF497', name=False):
       cols = get_cols(reg_data)
       logit = sm.Logit(reg_data[dependent_var], reg_data[cols])
       result = logit.fit()
       if name:
           print("Logit Regression for: ", name)
       print(result.summary())

Run the analysis
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

All variables
""""""""""""""""""""""""


.. code-block:: python
   :linenos:

   make_logit(prepare_data([2, 10]), name="EF497 (2) Gas")

>>>::

    Optimization terminated successfully.
             Current function value: 0.430748
             Iterations 12

    Logit Regression for:  EF497 (2) Gas

                               Logit Regression Results
    ==============================================================================
    Dep. Variable:                  EF497   No. Observations:               170163
    Model:                          Logit   Df Residuals:                   170123
    Method:                           MLE   Df Model:                           39
    Date:                Thu, 14 Jan 2016   Pseudo R-squ.:                  0.3770
    Time:                        23:20:47   Log-Likelihood:                -73297.
    converged:                       True   LL-Null:                   -1.1765e+05
                                            LLR p-value:                     0.000
    ==============================================================================
                     coef    std err          z      P>|z|      [95.0% Conf. Int.]
    ------------------------------------------------------------------------------
    EF501        4.34e-05   9.17e-06      4.731      0.000      2.54e-05  6.14e-05
    intercept    -10.0110      0.263    -38.051      0.000       -10.527    -9.495
    EF496_2.0      9.4434      0.259     36.426      0.000         8.935     9.951
    EF496_3.0      9.9689      0.260     38.273      0.000         9.458    10.479
    EF496_4.0      7.5797      0.261     28.989      0.000         7.067     8.092
    EF1_2         -0.4458      0.058     -7.639      0.000        -0.560    -0.331
    EF1_3          0.2839      0.042      6.704      0.000         0.201     0.367
    EF1_4          0.6221   5.21e+06    1.2e-07      1.000     -1.02e+07  1.02e+07
    EF1_5         -0.1103      0.039     -2.793      0.005        -0.188    -0.033
    EF1_6         -0.2062      0.044     -4.704      0.000        -0.292    -0.120
    EF1_7          0.0740      0.047      1.565      0.118        -0.019     0.167
    EF1_8         -0.6808      0.040    -17.224      0.000        -0.758    -0.603
    EF1_9         -0.6899      0.039    -17.841      0.000        -0.766    -0.614
    EF1_10        -0.5496      0.084     -6.577      0.000        -0.713    -0.386
    EF1_11        -1.9230      0.056    -34.510      0.000        -2.032    -1.814
    EF1_12         0.7490      0.067     11.122      0.000         0.617     0.881
    EF1_13         0.6904      0.078      8.802      0.000         0.537     0.844
    EF1_14         0.9761      0.051     19.301      0.000         0.877     1.075
    EF1_15         0.7580      0.060     12.720      0.000         0.641     0.875
    EF1_16         0.7340      0.060     12.192      0.000         0.616     0.852
    EF494_2.0      0.0110      0.026      0.419      0.675        -0.040     0.062
    EF494_3.0     -0.5442      0.021    -26.005      0.000        -0.585    -0.503
    EF494_4.0     -0.1865      0.029     -6.539      0.000        -0.242    -0.131
    EF494_5.0     -0.0414      0.049     -0.844      0.399        -0.138     0.055
    EF494_6.0      0.5484      0.039     13.985      0.000         0.472     0.625
    EF494_7.0      0.5297      0.059      9.036      0.000         0.415     0.645
    EF494_8.0      0.2494      0.057      4.367      0.000         0.137     0.361
    EF494_9.0      0.2047      0.117      1.756      0.079        -0.024     0.433
    EF563_2        0.9074      0.026     35.256      0.000         0.857     0.958
    EF563_3        1.4871      0.027     55.069      0.000         1.434     1.540
    EF563_4        1.7971      0.030     59.908      0.000         1.738     1.856
    EF563_5        1.7602      0.032     54.828      0.000         1.697     1.823
    EF563_6        0.6903      0.039     17.919      0.000         0.615     0.766
    EF563_7        1.8090      0.054     33.252      0.000         1.702     1.916
    EF563_8        0.6221   5.21e+06    1.2e-07      1.000     -1.02e+07  1.02e+07
    EF563_9        2.8746      0.069     41.830      0.000         2.740     3.009
    EF570_2        0.5801      0.016     35.527      0.000         0.548     0.612
    EF570_3        0.2826      0.021     13.414      0.000         0.241     0.324
    EF570_4        0.0367      0.182      0.202      0.840        -0.320     0.394
    EF570_6        0.4032      0.032     12.591      0.000         0.340     0.466
    EF570_7        0.2480      0.171      1.451      0.147        -0.087     0.583
    ==============================================================================


.. code-block:: python
   :linenos:

   make_logit(prepare_data([3, 10]),
              name="EF497 (3) Elektrizität, Strom (ohne Wärmepumpe)")

>>>::

    Optimization terminated successfully.
             Current function value: 0.039934
             Iterations 13

    Logit Regression for:  EF497 (3) Elektrizität, Strom (ohne Wärmepumpe)

                               Logit Regression Results
    ==============================================================================
    Dep. Variable:                  EF497   No. Observations:               170163
    Model:                          Logit   Df Residuals:                   170123
    Method:                           MLE   Df Model:                           39
    Date:                Thu, 14 Jan 2016   Pseudo R-squ.:                  0.6101
    Time:                        23:20:53   Log-Likelihood:                -6795.3
    converged:                       True   LL-Null:                       -17427.
                                            LLR p-value:                     0.000
    ==============================================================================
                     coef    std err          z      P>|z|      [95.0% Conf. Int.]
    ------------------------------------------------------------------------------
    EF501      -6.207e-05   1.69e-05     -3.681      0.000     -9.51e-05  -2.9e-05
    intercept     -9.6580      0.918    -10.524      0.000       -11.457    -7.859
    EF496_2.0      1.0058      0.386      2.606      0.009         0.249     1.762
    EF496_3.0      6.4062      0.362     17.686      0.000         5.696     7.116
    EF496_4.0      9.1527      0.362     25.316      0.000         8.444     9.861
    EF1_2          2.1710      0.987      2.199      0.028         0.236     4.106
    EF1_3          0.2999      0.788      0.381      0.704        -1.245     1.844
    EF1_4         -0.4384   1.47e+17  -2.98e-18      1.000     -2.89e+17  2.89e+17
    EF1_5          0.7322      0.929      0.788      0.431        -1.089     2.553
    EF1_6          0.5530      0.891      0.621      0.535        -1.194     2.300
    EF1_7         -0.1385      1.027     -0.135      0.893        -2.152     1.875
    EF1_8          0.2202      0.885      0.249      0.803        -1.514     1.954
    EF1_9         -0.4343      0.871     -0.498      0.618        -2.142     1.274
    EF1_10        -0.1665      0.871     -0.191      0.848        -1.873     1.540
    EF1_11         0.7245      0.867      0.835      0.404        -0.975     2.424
    EF1_12        -0.3730      0.896     -0.416      0.677        -2.129     1.383
    EF1_13        -0.2689      0.918     -0.293      0.770        -2.068     1.530
    EF1_14         0.5624      0.896      0.628      0.530        -1.194     2.319
    EF1_15        -0.2929      1.032     -0.284      0.777        -2.316     1.730
    EF1_16        -0.0015      0.953     -0.002      0.999        -1.868     1.865
    EF494_2.0      0.0487      0.073      0.665      0.506        -0.095     0.192
    EF494_3.0      0.5632      0.060      9.406      0.000         0.446     0.681
    EF494_4.0      1.1127      0.094     11.893      0.000         0.929     1.296
    EF494_5.0      0.3248      0.222      1.465      0.143        -0.110     0.759
    EF494_6.0      0.2213      0.185      1.196      0.232        -0.141     0.584
    EF494_7.0      0.9841      0.226      4.361      0.000         0.542     1.426
    EF494_8.0      0.2554      0.224      1.143      0.253        -0.183     0.693
    EF494_9.0      1.1179      0.434      2.575      0.010         0.267     1.969
    EF563_2        0.0078        nan        nan        nan           nan       nan
    EF563_3       -0.4235        nan        nan        nan           nan       nan
    EF563_4       -0.4633        nan        nan        nan           nan       nan
    EF563_5       -0.2361        nan        nan        nan           nan       nan
    EF563_6        0.0229        nan        nan        nan           nan       nan
    EF563_7       -0.3715      0.160     -2.321      0.020        -0.685    -0.058
    EF563_8       -0.4384   1.47e+17  -2.98e-18      1.000     -2.89e+17  2.89e+17
    EF563_9       -1.5148      0.596     -2.541      0.011        -2.683    -0.346
    EF570_2        0.0409      0.072      0.567      0.571        -0.101     0.182
    EF570_3        0.5517      0.128      4.324      0.000         0.302     0.802
    EF570_4        1.2042      0.509      2.365      0.018         0.206     2.202
    EF570_6        0.2354      0.118      1.998      0.046         0.005     0.466
    EF570_7        0.4834      0.637      0.759      0.448        -0.766     1.733
    ==============================================================================

    Possibly complete quasi-separation: A fraction 0.12 of observations can be
    perfectly predicted. This might indicate that there is complete
    quasi-separation. In this case some parameters will not be identified.

.. code-block:: python
   :linenos:

   make_logit(prepare_data([4, 10]), name="EF497 (4) Heizöl")

>>>::

    Optimization terminated successfully.
             Current function value: 0.416104
             Iterations 14

    Logit Regression for:  EF497 (4) Heizöl

                               Logit Regression Results
    ==============================================================================
    Dep. Variable:                  EF497   No. Observations:               170163
    Model:                          Logit   Df Residuals:                   170123
    Method:                           MLE   Df Model:                           39
    Date:                Thu, 14 Jan 2016   Pseudo R-squ.:                  0.2542
    Time:                        23:21:02   Log-Likelihood:                -70806.
    converged:                       True   LL-Null:                       -94935.
                                            LLR p-value:                     0.000
    ==============================================================================
                     coef    std err          z      P>|z|      [95.0% Conf. Int.]
    ------------------------------------------------------------------------------
    EF501       -6.89e-05   1.07e-05     -6.436      0.000     -8.99e-05 -4.79e-05
    intercept     -8.5331      0.586    -14.559      0.000        -9.682    -7.384
    EF496_2.0      8.7381      0.578     15.129      0.000         7.606     9.870
    EF496_3.0      7.6249      0.578     13.183      0.000         6.491     8.759
    EF496_4.0      6.9526      0.579     12.006      0.000         5.818     8.088
    EF1_2          0.1423      0.107      1.329      0.184        -0.068     0.352
    EF1_3         -0.3641      0.089     -4.087      0.000        -0.539    -0.189
    EF1_4         -0.4909   1.11e+16  -4.44e-17      1.000     -2.17e+16  2.17e+16
    EF1_5         -0.0784      0.090     -0.873      0.383        -0.255     0.098
    EF1_6          0.0546      0.101      0.541      0.589        -0.143     0.252
    EF1_7          0.0017      0.093      0.018      0.986        -0.181     0.184
    EF1_8          0.5539      0.083      6.668      0.000         0.391     0.717
    EF1_9          0.4684      0.096      4.889      0.000         0.281     0.656
    EF1_10         0.6372      0.069      9.290      0.000         0.503     0.772
    EF1_11         1.7415      0.112     15.546      0.000         1.522     1.961
    EF1_12        -0.9734      0.081    -12.088      0.000        -1.131    -0.816
    EF1_13        -0.8806      0.118     -7.460      0.000        -1.112    -0.649
    EF1_14        -1.1738      0.095    -12.351      0.000        -1.360    -0.988
    EF1_15        -0.8259      0.101     -8.160      0.000        -1.024    -0.628
    EF1_16        -0.7459      0.098     -7.582      0.000        -0.939    -0.553
    EF494_2.0      0.0497      0.028      1.790      0.073        -0.005     0.104
    EF494_3.0      0.6010      0.022     27.111      0.000         0.558     0.644
    EF494_4.0      0.1669      0.030      5.605      0.000         0.109     0.225
    EF494_5.0      0.1011      0.049      2.054      0.040         0.005     0.198
    EF494_6.0     -0.4236      0.042    -10.028      0.000        -0.506    -0.341
    EF494_7.0     -0.5904      0.063     -9.427      0.000        -0.713    -0.468
    EF494_8.0     -0.8600      0.070    -12.299      0.000        -0.997    -0.723
    EF494_9.0     -0.4823      0.131     -3.677      0.000        -0.739    -0.225
    EF563_2       -0.6361      0.024    -26.093      0.000        -0.684    -0.588
    EF563_3       -1.1008      0.018    -59.806      0.000        -1.137    -1.065
    EF563_4       -1.4118      0.020    -68.890      0.000        -1.452    -1.372
    EF563_5       -1.3293      0.017    -77.066      0.000        -1.363    -1.295
    EF563_6       -0.3459      0.028    -12.449      0.000        -0.400    -0.291
    EF563_7       -1.7449      0.068    -25.824      0.000        -1.877    -1.612
    EF563_8       -0.4909   1.11e+16  -4.44e-17      1.000     -2.17e+16  2.17e+16
    EF563_9       -2.4434      0.056    -43.364      0.000        -2.554    -2.333
    EF570_2       -0.4865      0.017    -28.678      0.000        -0.520    -0.453
    EF570_3       -0.2153      0.021    -10.053      0.000        -0.257    -0.173
    EF570_4        0.0517      0.182      0.284      0.777        -0.306     0.409
    EF570_6       -0.3854      0.020    -19.698      0.000        -0.424    -0.347
    EF570_7       -1.7335      0.307     -5.649      0.000        -2.335    -1.132
    ==============================================================================

    Possibly complete quasi-separation: A fraction 0.19 of observations can be
    perfectly predicted. This might indicate that there is complete
    quasi-separation. In this case some parameters will not be identified.


Specific "Heating Typ" variable
"""""""""""""""""""""""""""""""""""""""


.. code-block:: python
   :linenos:

   make_logit(prepare_data([10, 10]),
              name="EF497 (10) Erd- und andere Umweltwärme,
                    Abluftwärme (Wärmepumpen, -tauscher)")

>>>::

    Warning: Maximum number of iterations has been exceeded.
             Current function value: 0.016767
             Iterations: 35

    Logit Regression for:  EF497 (10) Erd- und andere Umweltwärme, Abluftwärme (Wärmepumpen, -tauscher)

                               Logit Regression Results
    ==============================================================================
    Dep. Variable:                  EF497   No. Observations:               170163
    Model:                          Logit   Df Residuals:                   170125
    Method:                           MLE   Df Model:                           37
    Date:                Thu, 14 Jan 2016   Pseudo R-squ.:                  0.1600
    Time:                        23:21:16   Log-Likelihood:                -2853.1
    converged:                      False   LL-Null:                       -3396.7
                                            LLR p-value:                4.086e-204
    ==============================================================================
                     coef    std err          z      P>|z|      [95.0% Conf. Int.]
    ------------------------------------------------------------------------------
    EF501       1.216e-05    5.8e-05      0.210      0.834        -0.000     0.000
    intercept     -8.7919        nan        nan        nan           nan       nan
    EF496          0.8589      0.077     11.159      0.000         0.708     1.010
    EF1_2          0.7707        nan        nan        nan           nan       nan
    EF1_3          0.6371        nan        nan        nan           nan       nan
    EF1_4          0.4789        nan        nan        nan           nan       nan
    EF1_5          1.0070        nan        nan        nan           nan       nan
    EF1_6          0.7295        nan        nan        nan           nan       nan
    EF1_7          0.4698        nan        nan        nan           nan       nan
    EF1_8          1.6309        nan        nan        nan           nan       nan
    EF1_9          2.2691        nan        nan        nan           nan       nan
    EF1_10         0.4633        nan        nan        nan           nan       nan
    EF1_11         1.2662        nan        nan        nan           nan       nan
    EF1_12         1.7382        nan        nan        nan           nan       nan
    EF1_13         0.7892        nan        nan        nan           nan       nan
    EF1_14         1.9578        nan        nan        nan           nan       nan
    EF1_15         1.3721        nan        nan        nan           nan       nan
    EF1_16         2.1385        nan        nan        nan           nan       nan
    EF494_2.0      0.2398      0.244      0.982      0.326        -0.239     0.718
    EF494_3.0      0.0623      0.320      0.194      0.846        -0.566     0.690
    EF494_4.0      0.7074      0.281      2.514      0.012         0.156     1.259
    EF494_5.0     -0.0032      0.352     -0.009      0.993        -0.693     0.686
    EF494_6.0     -0.1155      0.231     -0.501      0.617        -0.568     0.337
    EF494_7.0      1.1683      0.259      4.505      0.000         0.660     1.677
    EF494_8.0      2.7095      0.187     14.489      0.000         2.343     3.076
    EF494_9.0      1.9967      0.293      6.817      0.000         1.423     2.571
    EF563_2       -0.5886      0.145     -4.072      0.000        -0.872    -0.305
    EF563_3       -0.3005      0.161     -1.865      0.062        -0.616     0.015
    EF563_4       -0.7878      0.213     -3.700      0.000        -1.205    -0.371
    EF563_5       -1.3082      0.251     -5.212      0.000        -1.800    -0.816
    EF563_6        0.6663      0.265      2.518      0.012         0.148     1.185
    EF563_7       -1.0463      0.279     -3.754      0.000        -1.593    -0.500
    EF563_8        0.4789        nan        nan        nan           nan       nan
    EF563_9        0.1544      0.872      0.177      0.859        -1.555     1.864
    EF570_2       -0.5264        nan        nan        nan           nan       nan
    EF570_3       -1.3362      0.239     -5.583      0.000        -1.805    -0.867
    EF570_4      -19.9153   1.83e+04     -0.001      0.999     -3.58e+04  3.58e+04
    EF570_6        0.5191      0.082      6.307      0.000         0.358     0.680
    EF570_7        1.7147      0.344      4.983      0.000         1.040     2.389
    ==============================================================================


No "State" Variable
"""""""""""""""""""""""""""""""""""""""

No "Construction Year" variable
"""""""""""""""""""""""""""""""""""""""

.. code-block:: python
   :linenos:

   make_logit(prepare_data([6, 10]), name="EF497 (6) Koks, Steinkohle")

>>>::

    Warning: Maximum number of iterations has been exceeded.
             Current function value: 0.005796
             Iterations: 35

    Logit Regression for:  EF497 (6) Koks, Steinkohle

                               Logit Regression Results
    ==============================================================================
    Dep. Variable:                  EF497   No. Observations:               170163
    Model:                          Logit   Df Residuals:                   170131
    Method:                           MLE   Df Model:                           31
    Date:                Thu, 14 Jan 2016   Pseudo R-squ.:                  0.2702
    Time:                        23:21:26   Log-Likelihood:                -986.25
    converged:                      False   LL-Null:                       -1351.3
                                            LLR p-value:                1.276e-133
    ==============================================================================
                     coef    std err          z      P>|z|      [95.0% Conf. Int.]
    ------------------------------------------------------------------------------
    EF501          0.0001   3.53e-05      3.106      0.002      4.05e-05     0.000
    intercept    -42.5508   3506.865     -0.012      0.990     -6915.881  6830.779
    EF496_2.0     19.3922   4266.492      0.005      0.996     -8342.778  8381.562
    EF496_3.0     19.6385   4266.492      0.005      0.996     -8342.531  8381.808
    EF496_4.0     22.6205   4266.492      0.005      0.996     -8339.549  8384.790
    EF1_2         16.2291      0.616     26.351      0.000        15.022    17.436
    EF1_3         14.2216      0.381     37.345      0.000        13.475    14.968
    EF1_4          2.5748        nan        nan        nan           nan       nan
    EF1_5         16.0310        nan        nan        nan           nan       nan
    EF1_6         13.3715      0.665     20.117      0.000        12.069    14.674
    EF1_7         -6.4566   1.95e+04     -0.000      1.000     -3.82e+04  3.82e+04
    EF1_8         14.0762      0.116    121.347      0.000        13.849    14.304
    EF1_9         14.8121        nan        nan        nan           nan       nan
    EF1_10        18.7323        nan        nan        nan           nan       nan
    EF1_11        -4.3747      2e+04     -0.000      1.000     -3.93e+04  3.93e+04
    EF1_12        16.5029      0.309     53.348      0.000        15.897    17.109
    EF1_13        15.5109      1.026     15.117      0.000        13.500    17.522
    EF1_14        15.7542      0.197     80.025      0.000        15.368    16.140
    EF1_15        14.7811      0.617     23.965      0.000        13.572    15.990
    EF1_16        15.8239      0.413     38.322      0.000        15.015    16.633
    EF563_2        0.2374      0.435      0.546      0.585        -0.615     1.089
    EF563_3        1.2587      0.453      2.778      0.005         0.371     2.147
    EF563_4        1.4743      0.469      3.146      0.002         0.556     2.393
    EF563_5        0.4209      0.520      0.810      0.418        -0.598     1.440
    EF563_6        0.1892      0.607      0.312      0.755        -1.000     1.378
    EF563_7       -0.4386      0.570     -0.769      0.442        -1.556     0.679
    EF563_8        2.5748        nan        nan        nan           nan       nan
    EF563_9       -1.4244   7.86e+04  -1.81e-05      1.000     -1.54e+05  1.54e+05
    EF570_2       -1.3991      0.234     -5.989      0.000        -1.857    -0.941
    EF570_3       -1.4180      0.379     -3.745      0.000        -2.160    -0.676
    EF570_4        0.9840      1.186      0.829      0.407        -1.341     3.309
    EF570_6        0.0073      0.278      0.026      0.979        -0.538     0.553
    EF570_7      -18.2094   1.85e+04     -0.001      0.999     -3.62e+04  3.62e+04
    ==============================================================================

    Possibly complete quasi-separation: A fraction 0.45 of observations can be
    perfectly predicted. This might indicate that there is complete
    quasi-separation. In this case some parameters will not be identified.


No "Small Area Size" variable
"""""""""""""""""""""""""""""""""""""""

.. code-block:: python
   :linenos:

   make_logit(prepare_data([5, 10]), name="EF497 (5) Briketts, Braunkohle")

>>>::

    Warning: Maximum number of iterations has been exceeded.
             Current function value: 0.007209
             Iterations: 35

    Logit Regression for:  EF497 (5) Briketts, Braunkohle

                               Logit Regression Results
    ==============================================================================
    Dep. Variable:                  EF497   No. Observations:               170163
    Model:                          Logit   Df Residuals:                   170132
    Method:                           MLE   Df Model:                           30
    Date:                Thu, 14 Jan 2016   Pseudo R-squ.:                  0.4281
    Time:                        23:21:38   Log-Likelihood:                -1226.7
    converged:                      False   LL-Null:                       -2144.8
                                            LLR p-value:                     0.000
    ==============================================================================
                     coef    std err          z      P>|z|      [95.0% Conf. Int.]
    ------------------------------------------------------------------------------
    EF501          0.0001   2.66e-05      5.581      0.000      9.63e-05     0.000
    intercept     -9.2093      0.393    -23.444      0.000        -9.979    -8.439
    EF496          1.8567      0.069     26.936      0.000         1.722     1.992
    EF1_2         -2.0776      0.772     -2.692      0.007        -3.590    -0.565
    EF1_3         -2.1751      0.501     -4.342      0.000        -3.157    -1.193
    EF1_4        -14.5773    720.016     -0.020      0.984     -1425.782  1396.628
    EF1_5         -1.8859      0.328     -5.747      0.000        -2.529    -1.243
    EF1_6         -1.5403      0.380     -4.053      0.000        -2.285    -0.796
    EF1_7         -1.7351      0.458     -3.789      0.000        -2.633    -0.838
    EF1_8         -2.4011      0.380     -6.314      0.000        -3.147    -1.656
    EF1_9         -3.0099      0.401     -7.510      0.000        -3.795    -2.224
    EF1_10         0.3758      0.438      0.858      0.391        -0.482     1.234
    EF1_11        -2.4633      1.053     -2.339      0.019        -4.528    -0.399
    EF1_12         1.3227      0.331      3.998      0.000         0.674     1.971
    EF1_13         0.3964      0.399      0.992      0.321        -0.387     1.179
    EF1_14         0.6064      0.321      1.887      0.059        -0.024     1.236
    EF1_15        -0.6722      0.422     -1.594      0.111        -1.499     0.154
    EF1_16         0.5242      0.336      1.559      0.119        -0.135     1.183
    EF494_2.0      0.3813      0.157      2.433      0.015         0.074     0.688
    EF494_3.0     -0.4689      0.172     -2.732      0.006        -0.805    -0.132
    EF494_4.0     -2.9579      0.990     -2.987      0.003        -4.899    -1.017
    EF494_5.0      0.9858      0.378      2.608      0.009         0.245     1.727
    EF494_6.0     -0.6305      0.562     -1.123      0.262        -1.731     0.470
    EF494_7.0    -16.4926   2347.378     -0.007      0.994     -4617.269  4584.284
    EF494_8.0     -1.0934      1.035     -1.057      0.291        -3.121     0.935
    EF494_9.0    -19.3572   1.82e+04     -0.001      0.999     -3.56e+04  3.56e+04
    EF570_2       -0.2722      0.145     -1.876      0.061        -0.557     0.012
    EF570_3       -0.4188      0.260     -1.610      0.107        -0.929     0.091
    EF570_4        1.3187      0.852      1.547      0.122        -0.352     2.989
    EF570_6       -1.3947      0.509     -2.738      0.006        -2.393    -0.396
    EF570_7      -14.4425   2285.959     -0.006      0.995     -4494.840  4465.955
    ==============================================================================

    Possibly complete quasi-separation: A fraction 0.68 of observations can be
    perfectly predicted. This might indicate that there is complete
    quasi-separation. In this case some parameters will not be identified.


No "Building Size" variable
"""""""""""""""""""""""""""""""""""""""

.. code-block:: python
   :linenos:

   make_logit(prepare_data([1, 10]), name="EF497 (1) Fernwärme (bei Fernheizung)")

>>>::

    Warning: Maximum number of iterations has been exceeded.
             Current function value: 0.001124
             Iterations: 35

    Logit Regression for:  EF497 (1) Fernwärme (bei Fernheizung)

                               Logit Regression Results
    ==============================================================================
    Dep. Variable:                  EF497   No. Observations:               170163
    Model:                          Logit   Df Residuals:                   170138
    Method:                           MLE   Df Model:                           24
    Date:                Thu, 14 Jan 2016   Pseudo R-squ.:                  0.9980
    Time:                        23:21:47   Log-Likelihood:                -191.24
    converged:                      False   LL-Null:                       -93610.
                                            LLR p-value:                     0.000
    ==============================================================================
                     coef    std err          z      P>|z|      [95.0% Conf. Int.]
    ------------------------------------------------------------------------------
    EF501          0.0084      0.006      1.478      0.139        -0.003     0.019
    intercept    211.8920   1.07e+06      0.000      1.000     -2.11e+06  2.11e+06
    EF496       -154.5361   5.37e+05     -0.000      1.000     -1.05e+06  1.05e+06
    EF1_2        -18.5537   6117.483     -0.003      0.998      -1.2e+04   1.2e+04
    EF1_3        -17.4205   6117.501     -0.003      0.998      -1.2e+04   1.2e+04
    EF1_4        -25.8868   5.01e+12  -5.17e-12      1.000     -9.81e+12  9.81e+12
    EF1_5        -17.9074   6117.475     -0.003      0.998      -1.2e+04   1.2e+04
    EF1_6          8.7787        nan        nan        nan           nan       nan
    EF1_7        -20.4296   6117.436     -0.003      0.997      -1.2e+04   1.2e+04
    EF1_8        -18.1829   6117.455     -0.003      0.998      -1.2e+04   1.2e+04
    EF1_9        -16.4529   6117.526     -0.003      0.998      -1.2e+04   1.2e+04
    EF1_10        -4.4680   8433.383     -0.001      1.000     -1.65e+04  1.65e+04
    EF1_11        24.7549   1.48e+21   1.67e-20      1.000      -2.9e+21   2.9e+21
    EF1_12        11.5866    283.452      0.041      0.967      -543.969   567.142
    EF1_13       -12.8968   3.84e+05  -3.36e-05      1.000     -7.52e+05  7.52e+05
    EF1_14        19.5151   1.51e+15   1.29e-14      1.000     -2.96e+15  2.96e+15
    EF1_15        34.0981        nan        nan        nan           nan       nan
    EF1_16         0.1348    799.801      0.000      1.000     -1567.447  1567.716
    EF563_2      -33.6277        nan        nan        nan           nan       nan
    EF563_3      -33.7591        nan        nan        nan           nan       nan
    EF563_4      -33.2326        nan        nan        nan           nan       nan
    EF563_5      -33.8486        nan        nan        nan           nan       nan
    EF563_6      -12.8535        nan        nan        nan           nan       nan
    EF563_7       -4.2090   2.47e+04     -0.000      1.000     -4.84e+04  4.84e+04
    EF563_8      -25.8868   5.01e+12  -5.17e-12      1.000     -9.81e+12  9.81e+12
    EF563_9      -12.3079   1.48e+21  -8.31e-21      1.000      -2.9e+21   2.9e+21
    ==============================================================================

    Possibly complete quasi-separation: A fraction 0.91 of observations can be
    perfectly predicted. This might indicate that there is complete
    quasi-separation. In this case some parameters will not be identified.

.. code-block:: python
   :linenos:

   make_logit(prepare_data([7, 10]), name="EF497 (7) Holz, Holzpellets")

>>>::

    Warning: Maximum number of iterations has been exceeded.
             Current function value: 0.064461
             Iterations: 35

    Logit Regression for:  EF497 (7) Holz, Holzpellets

                               Logit Regression Results
    ==============================================================================
    Dep. Variable:                  EF497   No. Observations:               170163
    Model:                          Logit   Df Residuals:                   170130
    Method:                           MLE   Df Model:                           32
    Date:                Thu, 14 Jan 2016   Pseudo R-squ.:                  0.2451
    Time:                        23:22:00   Log-Likelihood:                -10969.
    converged:                      False   LL-Null:                       -14530.
                                            LLR p-value:                     0.000
    ==============================================================================
                     coef    std err          z      P>|z|      [95.0% Conf. Int.]
    ------------------------------------------------------------------------------
    EF501       5.531e-05   1.57e-05      3.533      0.000      2.46e-05   8.6e-05
    intercept     -4.0349        nan        nan        nan           nan       nan
    EF496          0.5838      0.017     34.720      0.000         0.551     0.617
    EF1_2          1.4513      0.279      5.204      0.000         0.905     1.998
    EF1_3          0.6802        nan        nan        nan           nan       nan
    EF1_4         -0.7423   1.54e+17  -4.83e-18      1.000     -3.02e+17  3.02e+17
    EF1_5          0.7040        nan        nan        nan           nan       nan
    EF1_6          0.6819        nan        nan        nan           nan       nan
    EF1_7          0.5075        nan        nan        nan           nan       nan
    EF1_8          1.6881        nan        nan        nan           nan       nan
    EF1_9          2.2783        nan        nan        nan           nan       nan
    EF1_10         0.9659        nan        nan        nan           nan       nan
    EF1_11        -0.5465      0.768     -0.712      0.477        -2.052     0.959
    EF1_12         0.2158        nan        nan        nan           nan       nan
    EF1_13         0.0331        nan        nan        nan           nan       nan
    EF1_14         0.3769        nan        nan        nan           nan       nan
    EF1_15         0.2930        nan        nan        nan           nan       nan
    EF1_16         0.2878        nan        nan        nan           nan       nan
    EF494_2.0     -0.4028      0.062     -6.532      0.000        -0.524    -0.282
    EF494_3.0     -1.0295      0.055    -18.680      0.000        -1.138    -0.922
    EF494_4.0     -0.8480      0.078    -10.888      0.000        -1.001    -0.695
    EF494_5.0     -1.3400      0.153     -8.744      0.000        -1.640    -1.040
    EF494_6.0     -1.7028      0.107    -15.964      0.000        -1.912    -1.494
    EF494_7.0     -0.8689      0.140     -6.212      0.000        -1.143    -0.595
    EF494_8.0      0.4197      0.101      4.150      0.000         0.221     0.618
    EF494_9.0      0.3646      0.212      1.719      0.086        -0.051     0.780
    EF563_2       -0.9660        nan        nan        nan           nan       nan
    EF563_3       -1.8077      0.063    -28.520      0.000        -1.932    -1.683
    EF563_4       -2.4784      0.094    -26.291      0.000        -2.663    -2.294
    EF563_5       -3.4922      0.126    -27.617      0.000        -3.740    -3.244
    EF563_6       -0.5240      0.130     -4.037      0.000        -0.778    -0.270
    EF563_7       -1.9666      0.161    -12.212      0.000        -2.282    -1.651
    EF563_8       -0.7423   1.54e+17  -4.83e-18      1.000     -3.02e+17  3.02e+17
    EF563_9      -42.0656   1.83e+08   -2.3e-07      1.000     -3.58e+08  3.58e+08
    ==============================================================================



.. code-block:: python
   :linenos:

   make_logit(prepare_data([8, 10]), name="EF497 (8) Biomasse (außer Holz), Biogas")

>>>::

    Warning: Maximum number of iterations has been exceeded.
             Current function value: 0.004406
             Iterations: 35

    Logit Regression for:  EF497 (8) Biomasse (außer Holz), Biogas

                               Logit Regression Results
    ==============================================================================
    Dep. Variable:                  EF497   No. Observations:               170163
    Model:                          Logit   Df Residuals:                   170152
    Method:                           MLE   Df Model:                           10
    Date:                Thu, 14 Jan 2016   Pseudo R-squ.:                 0.04388
    Time:                        23:22:07   Log-Likelihood:                -749.66
    converged:                      False   LL-Null:                       -784.07
                                            LLR p-value:                 7.539e-11
    ==============================================================================
                     coef    std err          z      P>|z|      [95.0% Conf. Int.]
    ------------------------------------------------------------------------------
    EF501         -0.0002      0.000     -0.384      0.701        -0.001     0.001
    intercept     -6.1329      0.371    -16.547      0.000        -6.859    -5.406
    EF496         -0.5420      0.272     -1.990      0.047        -1.076    -0.008
    EF494_2.0     -1.1276      0.432     -2.611      0.009        -1.974    -0.281
    EF494_3.0     -1.4420      0.292     -4.933      0.000        -2.015    -0.869
    EF494_4.0     -1.6751      0.542     -3.089      0.002        -2.738    -0.612
    EF494_5.0      0.0399      0.459      0.087      0.931        -0.860     0.940
    EF494_6.0      0.3789      0.293      1.293      0.196        -0.195     0.953
    EF494_7.0    -10.9879    133.129     -0.083      0.934      -271.916   249.940
    EF494_8.0      0.2821      0.542      0.520      0.603        -0.781     1.345
    EF494_9.0    -11.1658    331.917     -0.034      0.973      -661.711   639.380
    ==============================================================================

.. code-block:: python
   :linenos:

   make_logit(prepare_data([9, 10]),
              name="EF497 (9) Sonnenenergie (Solarkollektoren)")

>>>::

    Warning: Maximum number of iterations has been exceeded.
             Current function value: 0.003209
             Iterations: 35

    Logit Regression for:  EF497 (9) Sonnenenergie (Solarkollektoren)

                               Logit Regression Results
    ==============================================================================
    Dep. Variable:                  EF497   No. Observations:               170163
    Model:                          Logit   Df Residuals:                   170135
    Method:                           MLE   Df Model:                           27
    Date:                Thu, 14 Jan 2016   Pseudo R-squ.:                 0.09011
    Time:                        23:22:18   Log-Likelihood:                -546.01
    converged:                      False   LL-Null:                       -600.09
                                            LLR p-value:                 1.140e-11
    ==============================================================================
                     coef    std err          z      P>|z|      [95.0% Conf. Int.]
    ------------------------------------------------------------------------------
    EF501         -0.0008      0.003     -0.308      0.758        -0.006     0.004
    intercept    -62.5399   9.89e+06  -6.32e-06      1.000     -1.94e+07  1.94e+07
    EF496_2.0     20.0470   5632.403      0.004      0.997      -1.1e+04  1.11e+04
    EF496_3.0     18.2855   5632.403      0.003      0.997      -1.1e+04  1.11e+04
    EF496_4.0     19.4810   5632.403      0.003      0.997      -1.1e+04  1.11e+04
    EF1_2         34.7815   9.89e+06   3.52e-06      1.000     -1.94e+07  1.94e+07
    EF1_3         35.7888   9.89e+06   3.62e-06      1.000     -1.94e+07  1.94e+07
    EF1_4         37.6147   9.89e+06    3.8e-06      1.000     -1.94e+07  1.94e+07
    EF1_5         35.1540   9.89e+06   3.55e-06      1.000     -1.94e+07  1.94e+07
    EF1_6         34.1001   9.89e+06   3.45e-06      1.000     -1.94e+07  1.94e+07
    EF1_7         34.9092   9.89e+06   3.53e-06      1.000     -1.94e+07  1.94e+07
    EF1_8         35.5632   9.89e+06   3.59e-06      1.000     -1.94e+07  1.94e+07
    EF1_9         35.5996   9.89e+06    3.6e-06      1.000     -1.94e+07  1.94e+07
    EF1_10       -16.3893   2.26e+11  -7.26e-11      1.000     -4.42e+11  4.42e+11
    EF1_11        34.1604   9.89e+06   3.45e-06      1.000     -1.94e+07  1.94e+07
    EF1_12        -6.5408   1.03e+09  -6.33e-09      1.000     -2.03e+09  2.03e+09
    EF1_13        28.5835   9.89e+06   2.89e-06      1.000     -1.94e+07  1.94e+07
    EF1_14       -18.5835   2.35e+11  -7.91e-11      1.000     -4.61e+11  4.61e+11
    EF1_15        35.7354   9.89e+06   3.61e-06      1.000     -1.94e+07  1.94e+07
    EF1_16        -6.5082   8.81e+08  -7.39e-09      1.000     -1.73e+09  1.73e+09
    EF494_2.0     -0.4353      0.571     -0.763      0.446        -1.554     0.684
    EF494_3.0     -0.1394      0.397     -0.351      0.725        -0.918     0.639
    EF494_4.0     -1.2784      0.794     -1.610      0.107        -2.835     0.278
    EF494_5.0      0.0047      0.797      0.006      0.995        -1.558     1.567
    EF494_6.0      0.0318      0.524      0.061      0.952        -0.995     1.059
    EF494_7.0      0.3281      0.795      0.413      0.680        -1.230     1.886
    EF494_8.0      1.4965      0.545      2.744      0.006         0.428     2.565
    EF494_9.0    -19.0125    2.6e+04     -0.001      0.999      -5.1e+04   5.1e+04
    ==============================================================================

    Possibly complete quasi-separation: A fraction 0.40 of observations can be
    perfectly predicted. This might indicate that there is complete
    quasi-separation. In this case some parameters will not be identified.



.. [BMU] OEKO (Oeko-Institut - Institute for Applied Ecology) et al. 2012:
   Update of Life-Cycle Data for Renewable Energies for GHG and Air Emissions;
   project FKZ 0325188 sponsored by BMU; Darmstadt.

.. [EVA] Energieverwertungs-Agentur 2002: Vergleich der Umweltauswirkungen
   einer Pelletheizung mit dennen konventioneller Energiebereitstellungssysteme
   am eispiel einer 400 kW Heizanlage. C. Rakos, H. Tretter; Wien

.. [Fichtner] Fichtner 2002: Erarbeitung von energetischen ung ökonomischen
   Kenndaten zur Bioenergie, Bericht i.A. des Öko-Instituts im Rahmen des
   Projekts "Stoffstromanalyse zur nachhaltigen energetischen Nutzung von
   Biomasse", Stuttgart.

.. [Nussbaumer] Thomas Nussbaumer 2002: Holzenergie - Teil 2b: Holzpellets und

   Pelletheizungen, in: Schweizer Baudokumentation (56) 101.
.. [OEKO.c] Öko-Institut (Institute for applied Ecology) /
   Fhl-UMSICHT (Fraunhofer-Institute for Environmental and Safety Technology)
   2003: Future Technologies; Working paper and Excel data sheets prepared in
   the project "Material Flow Analysis of Sustainable Biomass for Energy",
   Darmstadt/Oberhausen.

.. [OEKO.b] OEKO (Öko-Institut) 1994b: Umweltanalyse von Energie-, Transport-
   und Stoffsystemen: Gesamt-Emissions-Modell integrierter Systeme (GEMIS)
   Version 2.1 - erweiterter und aktualisierter Endbericht, U. Fritsche u.a.,
   i.A. des Hessischen Ministeriums für Umwelt, Energie und
   Bundesangelegenheiten (HMUEB), veröffentlicht durch HMUEB, Wiesbaden 1995.

.. [IVD] Institut für Verfahrenstechnik und Dampfkesselwesen. Universität
   Stuttgart (IVD) 2000: Ermittung der mittleren Emissionsfaktoren zur
   Darstellung der Emissionsentwicklung aus Feuerungsanlagen im Bereich der
   Haushalte und Kleinverbraucher. F. Pfeiffer, M. Stuschka, G. Baumbach. i.A.
   des UBA, Reihe Texte 1400, Berlin.

.. [BMWi.a] (Bundesministerium für Wirtschaft und Technologie) 2011:
   Energiedaten - Stromerzeugung in Deutschland (Excel-Datei).

.. [AGEEStat] (Arbeitsgemeinschaft Erneuerbare-Energien Statistik) 2014: Data
   for renewable energies 2013.

.. [BMWi.b] (Bundesministerium für Wirtschaft und Technologie) 2014:
   Energiedaten; Berlin.

.. [BMWi.c] (Bundesministerium für Wirtschaft und Technologie) 2014b:
   Zeitreihen zur Entwicklung der Erneuerbaren Energien in Deutschland; Berlin.

.. [GEMIS] OEKO (Öko-Institut) 1989: Core data base of the GEMIS project,
   continuous updating and extension of the GEMIS database for energy,
   materials, and transport.

.. [OEKO] OEKO (Öko-Institut) 1994: Umweltanalyse von Energie-, Transport- und
   Stoffsystemen: Gesamt-Emissions-Modell integrierter Systeme (GEMIS) Version
   2.2 - erweiterter und aktualisierter Endbericht, U. Fritsche u.a., i.A. des
   Hessischen Ministeriums für Umwelt, Energie und Bundesangelegenheiten
   (HMUEB), veröffentlicht durch HMUEB, Wiesbaden 1995.

.. [AGFW] Arbeitsgemeinschaft Fernwärme (AGFW) 2007: Main report on district
   heating 2006; Frankfurt
