#!/usr/bin/env python
# -*- coding:utf -*-
"""
#Created by Esteban.

Thu 28 Jul 2016 06:16:38 PM CEST

"""

import pandas as pd
import numpy as np
import os
import sys
# internal
from root.PrepareData.parseSurvey import Survey
from root.RHeat.userParam import getUserparam
from root.RHeat.computeHeat import computeHeat
from root.Synthetic.microToBtyp import defineBtyp

case = sys.argv[1]

SAVE_DIR = "results_heat_{}".format(case)
USE_MICRO = "survey_fp_{}".format(case)

dataS = Survey(surveyPrefix="_fp_raw", debug=False, static=True)
sur = dataS.getSurvey()
btyp_survey = defineBtyp(sur)
btyp_survey.whours = btyp_survey.whours / 5
btyp_survey['user_par'] = btyp_survey.whours.apply(lambda x: getUserparam(x))
btyp_survey = btyp_survey.loc[btyp_survey.whours.notnull()]

if not os.path.isfile("data/survey_fp.csv"):
    dataSurvey = Survey(surveyPrefix="_fp", saveCensus=True, static=False)
raw_survey = pd.read_csv("data/{}.csv".format(USE_MICRO))
raw_survey = raw_survey.loc[raw_survey.whours.notnull()]

raw_survey_all = pd.merge(
    raw_survey, btyp_survey.loc[:,["sqm", "ndu", "btyp", "user_par", "hhsize"]], how='inner',
    right_index=True, left_index=True, suffixes=('_mz', ''))

L = pd.read_csv("data/locations.csv")

#raw_survey_all = raw_survey_all.ix[range(10)]

for inx, region in L.iterrows():

    climate = region[1]
    file_name = "{}/survey-heat-{}.csv".format(SAVE_DIR, climate)

    if not os.path.isfile(file_name):

        bl = region[2:].tolist()
        bl = [int(a) for a in bl if not np.isnan(a)]
        if len(bl) > 1:
            inx_filter = [a in bl for a in raw_survey_all.NUTS2]
        else:
            inx_filter = (raw_survey_all.NUTS2 == bl[0])
        survey = raw_survey_all.loc[inx_filter, :]

        print("compute heat for {} with {} records".format(
            climate, survey.shape[0]))
        survey_out = computeHeat(
            survey,
            climate=climate,
            output="Year",
            fuel=True,
            user=True,
            #debug=True
            )
        print("OK")

        print("save data...", end="")
        if not os.path.isdir(SAVE_DIR):
            os.mkdir(SAVE_DIR)
        survey_out.to_csv(file_name)
        print("OK\n")

    else:
        print("skip")
