#!/usr/bin/env python
# -*- coding:utf -*-
"""
#Created by Esteban.

Sat 30 Jul 2016 06:04:53 PM CEST

"""


from root.Climate.ResultsGeo import Map

UN = "Wh/ha*a"
UC = "heat"
F  = 10
IC = 2

def plotMap(sf):
    M = Map(
        heatData="./results_merge/heat_{}.csv".format(sf),
        index_col=IC, unit=UN, factor=F, useCol=UC,
            )
    M.makeYear(sufix=sf)

def main():
    #scenarions = ["base"]
    scenarions = ["base", "s1", "s2", "s3"]
    for s in scenarions:
        plotMap(s)
    # plotMap("./results_merge/heat_s1.csv", "")
    # plotMap("./results_merge/heat_s2.csv")
    # plotMap("./results_merge/heat_s3.csv")

if __name__ == "__main__":
    main()
