#!/usr/bin/env python
# -*- coding:utf -*-
"""
#Created by Esteban.

Thu 14 Jul 2016 03:49:38 PM CEST

"""
import statsmodels.api as sm


def getCols(reg_data):
    cols = reg_data.columns
    cols = cols.delete(1)
    cols = cols[:reg_data.shape[1] -1]
    return cols

def makeLogit(reg_data, dependent_var='EF497', name=False):
    cols = getCols(reg_data)
    logit = sm.Logit(reg_data[dependent_var], reg_data[cols])
    result = logit.fit()
    if name:
        print("Logit Regression for: ", name)
    print(result.summary())
    return result
