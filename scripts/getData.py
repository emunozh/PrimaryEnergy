#!/usr/bin/env python
# -*- coding:utf -*-
"""
#Created by Esteban.

Thu 14 Jul 2016 02:07:01 PM CEST

"""
import pandas as pd
import numpy as np

#getCols_Start
def getCols(regData):
    cols = regData.columns
    cols = cols.delete(1)
    cols = cols[:regData.shape[1] -1]
    return cols

#Data_Start
class Data():

    def __init__(self, filePath="data/suf2010.csv"):

        # We load the raw data from the Microcensus with the variables we
        # identify as relevant for the analysis, see metadata for the variable
        # codes.
        self.rawData = pd.read_csv(
            filePath,              
            #nrows=10,
            usecols=["EF1",    # State
                     "EF494",  # Construction Year
                     "EF496",  # Heating typ
                     "EF501",  # Heat Expenditure
                     "EF563",  # Small Area size
                     "EF570",  # Building size
                     "EF497"], # Energy Source
            na_values=["(M)-1", "(M)-5", -1, -5])

        # Define nan values on the dataset.
        self.rawData["EF494"] = self.rawData.EF494.replace(99, np.nan)
        self.rawData["EF497"] = self.rawData.EF497.replace(99, np.nan)
        self.rawData["EF496"] = self.rawData.EF496.replace(9, np.nan)

        # And delete observations containing unknown observations/nans from the
        # dataset.
        self.rawData.dropna(inplace=True)

    def prepareData(self, val, dependentVar='EF497'):
        regData = self.rawData.loc[:, ['EF501', 'EF497']]
        regData['intercept'] = 1.0
        A = [a for a in range(1, val[1] +1)]
        A.remove(val[0])
        regData[dependentVar] = regData.loc[:, dependentVar].replace(A, 0)
        regData[dependentVar] = regData.loc[:, dependentVar].replace(val[0], 1)
        if val[0] in [1,5,6,7,8,9,10]:
            if val[0] == 1:                                                      ## (1) Fernwärme
                dummy_ef496 = self.rawData.EF496.replace([2,3,4], 0)
                dummy_ef496 = self.rawData.EF496.replace(1, 1)                        # (A) Fernheizung
                regData = regData.join(dummy_ef496)
                dummy_ef1   = pd.get_dummies(self.rawData['EF1'],   prefix='EF1'  )   # (B) State
                regData = regData.join(dummy_ef1.ix[:, 'EF1_2':])
                dummy_ef563 = pd.get_dummies(self.rawData['EF563'], prefix='EF563')   # (D) Small Area size
                regData = regData.join(dummy_ef563.ix[:, 'EF563_2':])
            elif val[0] == 5:                                                    ## (5) Heizöl
                dummy_ef496 = self.rawData.EF496.replace([1,3,4], 0)
                dummy_ef496 = self.rawData.EF496.replace(2, 1)                        # (A) Blockheizung, Zentralheizung
                regData = regData.join(dummy_ef496)
                dummy_ef1   = pd.get_dummies(self.rawData['EF1'],   prefix='EF1'  )   # (B) State
                regData = regData.join(dummy_ef1.ix[:, 'EF1_2':])
                dummy_ef494 = pd.get_dummies(self.rawData['EF494'], prefix='EF494')   # (C) Construction Year
                regData = regData.join(dummy_ef494.ix[:, 'EF494_2.0':])
                dummy_ef570 = pd.get_dummies(self.rawData['EF570'], prefix='EF570')   # (E) Building size
                regData = regData.join(dummy_ef570.ix[:, 'EF570_2':])
            elif val[0] == 6:                                                    ## (6) Koks, Steinkohle
                dummy_ef496 = pd.get_dummies(self.rawData['EF496'], prefix='EF496')   # (A) Heating typ - All
                regData = regData.join(dummy_ef496.ix[:, 'EF496_2.0':])
                dummy_ef1   = pd.get_dummies(self.rawData['EF1'],   prefix='EF1'  )   # (B) State
                regData = regData.join(dummy_ef1.ix[:, 'EF1_2':])
                dummy_ef563 = pd.get_dummies(self.rawData['EF563'], prefix='EF563')   # (D) Small Area size
                regData = regData.join(dummy_ef563.ix[:, 'EF563_2':])
                dummy_ef570 = pd.get_dummies(self.rawData['EF570'], prefix='EF570')   # (E) Building size
                regData = regData.join(dummy_ef570.ix[:, 'EF570_2':])
            elif val[0] == 7:                                                    ## (7) Holz, Holzpellets
                dummy_ef496 = self.rawData.EF496.replace([1,3,4], 0)
                dummy_ef496 = self.rawData.EF496.replace(2, 1)                        # (A) Blockheizung, Zentralheizung
                regData = regData.join(dummy_ef496)
                dummy_ef1   = pd.get_dummies(self.rawData['EF1'],   prefix='EF1'  )   # (B) State
                regData = regData.join(dummy_ef1.ix[:, 'EF1_2':])
                dummy_ef494 = pd.get_dummies(self.rawData['EF494'], prefix='EF494')   # (C) Construction Year
                regData = regData.join(dummy_ef494.ix[:, 'EF494_2.0':])
                dummy_ef563 = pd.get_dummies(self.rawData['EF563'], prefix='EF563')   # (D) Small Area size
                regData = regData.join(dummy_ef563.ix[:, 'EF563_2':])
            elif val[0] == 8:                                                    ## (8) Biomasse (außer Holz), Biogas
                dummy_ef496 = self.rawData.EF496.replace([1,3,4], 0)
                dummy_ef496 = self.rawData.EF496.replace(2, 1)                        # (A) Blockheizung, Zentralheizung
                regData = regData.join(dummy_ef496)
                dummy_ef494 = pd.get_dummies(self.rawData['EF494'], prefix='EF494')   # (C) Construction Year
                regData = regData.join(dummy_ef494.ix[:, 'EF494_2.0':])
            elif val[0] == 9:                                                    ## (9) Sonnenenergie (Solarkollektoren)
                dummy_ef496 = pd.get_dummies(self.rawData['EF496'], prefix='EF496')   # (A) Heating typ - All
                regData = regData.join(dummy_ef496.ix[:, 'EF496_2.0':])
                dummy_ef1   = pd.get_dummies(self.rawData['EF1'],   prefix='EF1'  )   # (B) State
                regData = regData.join(dummy_ef1.ix[:, 'EF1_2':])
                dummy_ef494 = pd.get_dummies(self.rawData['EF494'], prefix='EF494')   # (C) Construction Year
                regData = regData.join(dummy_ef494.ix[:, 'EF494_2.0':])
            elif val[0] == 10:                                                   ## (10) Wärmepumpen
                dummy_ef496 = self.rawData.EF496.replace([1,2,3], 0)
                dummy_ef496 = self.rawData.EF496.replace(4, 1)                        # (A) Einzel- oder Mehrraumöfen (auch Elektrospeicher)
                regData = regData.join(dummy_ef496)
                dummy_ef1   = pd.get_dummies(self.rawData['EF1'],   prefix='EF1'  )   # (B) State
                regData = regData.join(dummy_ef1.ix[:, 'EF1_2':])
                dummy_ef494 = pd.get_dummies(self.rawData['EF494'], prefix='EF494')   # (C) Construction Year
                regData = regData.join(dummy_ef494.ix[:, 'EF494_2.0':])
                dummy_ef563 = pd.get_dummies(self.rawData['EF563'], prefix='EF563')   # (D) Small Area size
                regData = regData.join(dummy_ef563.ix[:, 'EF563_2':])
                dummy_ef570 = pd.get_dummies(self.rawData['EF570'], prefix='EF570')   # (E) Building size
                regData = regData.join(dummy_ef570.ix[:, 'EF570_2':])
        else:
            dummy_ef496 = pd.get_dummies(self.rawData['EF496'], prefix='EF496')       # (A) Heating typ - All
            regData = regData.join(dummy_ef496.ix[:, 'EF496_2.0':])
            dummy_ef1   = pd.get_dummies(self.rawData['EF1'],   prefix='EF1'  )       # (B) State
            regData = regData.join(dummy_ef1.ix[:, 'EF1_2':])
            dummy_ef494 = pd.get_dummies(self.rawData['EF494'], prefix='EF494')       # (C) Construction Year
            regData = regData.join(dummy_ef494.ix[:, 'EF494_2.0':])
            dummy_ef563 = pd.get_dummies(self.rawData['EF563'], prefix='EF563')       # (D) Small Area size
            regData = regData.join(dummy_ef563.ix[:, 'EF563_2':])
            dummy_ef570 = pd.get_dummies(self.rawData['EF570'], prefix='EF570')       # (E) Building size
            regData = regData.join(dummy_ef570.ix[:, 'EF570_2':])

        return regData
